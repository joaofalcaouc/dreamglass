﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DreamWorldDLL;
using System.IO;

/**
 * RigSetup has the information about eyes and calibration parameters
 * This is where eye vision is defined and how it's going to be seen by the user
 * 
 **/
[Serializable]
class glasses_int_ext
{
    public double[] Ext_l;
    public double[] Ext_r;
    public double[] Int_l;
    public double[] Int_r;
};
public class RigSetup : MonoBehaviour {

    private Camera editorCam;//Virtual Camera for editor
    private Camera rtLeftCam;//Camera of left eye
    private Camera rtRightCam;//Camera of right eye
    private Material leftMeshMat;
    private Material rightMeshMat;

    private CalibrationData pcPlugin;
    private AndroidCalibration androidPlugin;

    private Quaternion cameraRotation = Quaternion.Euler(0, 0, 0);
    private RenderTexture rtLeft;
    private RenderTexture rtRight;
    private float ipd;//Half distance between the eyes LEye<-ipd-C-ipd->REye
    private Vector3 leftEyeCamPos;
    private Vector3 rightEyeCamPos;
    private Vector2 rtResolutionPC;// Resolution of the displaying plane
    private Vector2 rtResolutionAndroid;
    private float cameraTilt;
    private float fov; // Field of view 
    private float eyeHeight = 0.15f;//0.15f; // Height of the eyes
    private float eyeDepth = 0.1f;//0.1f; //Depth of the eyes
    private int platform;//Platform of work (pc or android)
    private int tracking;
    private Matrix4x4 setup_t_l;
    private Matrix4x4 setup_t_r;
    private Matrix4x4 setup_m_l;
    private Matrix4x4 setup_m_r;
    private glasses_int_ext setup_calib;
    public  bool fab_calib;

    //Function to read from the json calibration file
    void read_int_ext(string path_file)
    {
        if (File.Exists(path_file))
        {
            string jsonddata = File.ReadAllText(path_file);
            setup_calib = JsonUtility.FromJson<glasses_int_ext>(jsonddata);
        }
        else
        {
            Debug.LogError("Cannot open file for calibration data");
        }
    }
    //Initialize the variables to be used in the unity cameras
    public void load_calibration(double []calib_el, double[] calib_er, double[] calib_il, double[] calib_ir)
    {
        setup_t_l.m00 = (float)calib_el[0]; setup_t_l.m01 = (float)calib_el[1]; setup_t_l.m02 = (float)calib_el[2]; setup_t_l.m03 = (float)calib_el[3];
        setup_t_l.m10 = (float)calib_el[4]; setup_t_l.m11 = (float)calib_el[5]; setup_t_l.m12 = (float)calib_el[6]; setup_t_l.m13 = (float)calib_el[7];
        setup_t_l.m20 = (float)calib_el[8]; setup_t_l.m21 = (float)calib_el[9]; setup_t_l.m22 = (float)calib_el[10]; setup_t_l.m23 = (float)calib_el[11];
        setup_t_l.m30 = (float)calib_el[12]; setup_t_l.m31 = (float)calib_el[13]; setup_t_l.m32 = (float)calib_el[14]; setup_t_l.m33 = (float)calib_el[15];

        setup_t_r.m00 = (float)calib_er[0]; setup_t_r.m01 = (float)calib_er[1]; setup_t_r.m02 = (float)calib_er[2]; setup_t_r.m03 = (float)calib_er[3];
        setup_t_r.m10 = (float)calib_er[4]; setup_t_r.m11 = (float)calib_er[5]; setup_t_r.m12 = (float)calib_er[6]; setup_t_r.m13 = (float)calib_er[7];
        setup_t_r.m20 = (float)calib_er[8]; setup_t_r.m21 = (float)calib_er[9]; setup_t_r.m22 = (float)calib_er[10]; setup_t_r.m23 = (float)calib_er[11];
        setup_t_r.m30 = (float)calib_er[12]; setup_t_r.m31 = (float)calib_er[13]; setup_t_r.m32 = (float)calib_er[14]; setup_t_r.m33 = (float)calib_er[15];

        setup_m_l.m00 = (float)calib_il[0]; setup_m_l.m01 = (float)calib_il[1]; setup_m_l.m02 = (float)calib_il[2]; setup_m_l.m03 = (float)calib_il[3];
        setup_m_l.m10 = (float)calib_il[4]; setup_m_l.m11 = (float)calib_il[5]; setup_m_l.m12 = (float)calib_il[6]; setup_m_l.m13 = (float)calib_il[7];
        setup_m_l.m20 = (float)calib_il[8]; setup_m_l.m21 = (float)calib_il[9]; setup_m_l.m22 = (float)calib_il[10]; setup_m_l.m23 = (float)calib_il[11];
        setup_m_l.m30 = (float)calib_il[12]; setup_m_l.m31 = (float)calib_il[13]; setup_m_l.m32 = (float)calib_il[14]; setup_m_l.m33 = (float)calib_il[15];
        //Matrix4x4 setup_m_r;
        setup_m_r.m00 = (float)calib_ir[0]; setup_m_r.m01 = (float)calib_ir[1]; setup_m_r.m02 = (float)calib_ir[2]; setup_m_r.m03 = (float)calib_ir[3];
        setup_m_r.m10 = (float)calib_ir[4]; setup_m_r.m11 = (float)calib_ir[5]; setup_m_r.m12 = (float)calib_ir[6]; setup_m_r.m13 = (float)calib_ir[7];
        setup_m_r.m20 = (float)calib_ir[8]; setup_m_r.m21 = (float)calib_ir[9]; setup_m_r.m22 = (float)calib_ir[10]; setup_m_r.m23 = (float)calib_ir[11];
        setup_m_r.m30 = (float)calib_ir[12]; setup_m_r.m31 = (float)calib_ir[13]; setup_m_r.m32 = (float)calib_ir[14]; setup_m_r.m33 = (float)calib_ir[15];
    }

    public void guarda()//Saves variables in file for check up
    {
        using (StreamWriter sw = new StreamWriter("Cam_Data.txt", false))//Path.Combine(mydocpath, "TestFile.txt")
        {
            /*sw.Write(rtResolutionPC.x);
            sw.Write("\r\n");
            sw.Write(rtResolutionPC.y);
            sw.Write("\r\n");
            sw.Write(fov);
            sw.Write("\r\n");
            sw.Write("rtLeftCam.depth");
            sw.Write("\r\n");
            sw.Write(rtLeftCam.depth);
            sw.Write("\r\n");
            sw.Write(rtRightCam.aspect);
            sw.Write("\r\n");
            sw.Write(rtRightCam.focalLength);
            sw.Write("\r\n");
            sw.Write(rtRightCam.nearClipPlane);
            sw.Write("\r\n");
            sw.Write(rtRightCam.farClipPlane);
            sw.Write("\r\n");
            sw.Write("rtRightCam.cameraToWorldMatrix");
            sw.Write("\r\n");
            sw.Write(rtRightCam.cameraToWorldMatrix);
            sw.Write("\r\n");
            sw.Write("rtLeftCam.cameraToWorldMatrix");
            sw.Write("\r\n");
            sw.Write(rtLeftCam.cameraToWorldMatrix);
            sw.Write("\r\n");
            sw.Write(rtRightCam.projectionMatrix);
            sw.Write("\r\n");
            sw.Write(rtLeftCam.projectionMatrix);
            sw.Write("\r\n");
            sw.Write(rtRightCam.orthographic);
            sw.Write("\r\n");
            
            sw.Write(leftEyeCamPos);
            sw.Write("\r\n");
            sw.Write(rightEyeCamPos);
            sw.Write("\r\n");

            sw.Write("pcPlugin.IPD()");
            sw.Write("\r\n");
            sw.Write(pcPlugin.IPD());
            sw.Write("\r\n");
            sw.Write("pcPlugin.RTRez()");
            sw.Write("\r\n");
            sw.Write(pcPlugin.RTRez());
            sw.Write("\r\n");
            sw.Write("pcPlugin.Tilt()");
            sw.Write("\r\n");
            sw.Write(pcPlugin.Tilt());
            sw.Write("\r\n");
            sw.Write("pcPlugin.FOV()");
            sw.Write("\r\n");
            sw.Write(pcPlugin.FOV());
            sw.Write("\r\n");
            sw.Write("pcPlugin.LeftCamPos()");
            sw.Write("\r\n");
            sw.Write(pcPlugin.LeftCamPos());
            sw.Write("\r\n");*/

            sw.Write(rtLeftCam.nearClipPlane);
            sw.Write("\r\n");
            sw.Write(rtLeftCam.farClipPlane);
            sw.Write("\r\n");

            //Trying to get position of screens
            sw.Write("this.gameObject.transform.position");
            sw.Write("\r\n");
            sw.Write(this.transform.localPosition);
            sw.Write("\r\n");
            sw.Write(rtLeftCam.transform.localPosition);
            sw.Write("\r\n");
            sw.Write(rtLeftCam.transform.worldToLocalMatrix);
            sw.Write("\r\n");

            //Data
            sw.Write("ProjectionMatrix");
            sw.Write("\r\n");
            sw.Write(rtLeftCam.projectionMatrix);
            sw.Write("\r\n");
            sw.Write("ProjectionMatrix");
            sw.Write("\r\n");
            sw.Write(rtRightCam.projectionMatrix);
            sw.Write("\r\n");
            sw.Write("rtLeftCam.worldToCameraMatrix");
            sw.Write("\r\n");
            sw.Write(rtLeftCam.worldToCameraMatrix);
            sw.Write("\r\n");
            sw.Write("rtRightCam.worldToCameraMatrix");
            sw.Write("\r\n");
            sw.Write(rtRightCam.worldToCameraMatrix);
            sw.Write("\r\n");

            Vector4 exp;
            sw.Write("Height and width");
            sw.Write("\r\n");
            sw.Write(rtLeftCam.scaledPixelHeight);
            sw.Write("\r\n");
            sw.Write(rtLeftCam.scaledPixelWidth);
            sw.Write("\r\n");

            Vector3 aux;
            aux = new Vector3(0.0f, 0.0f, 0.0f);
            int i;
            float p1 = 0.0f;
            float c1 = 0.05f;

            for (i = 0; i < 5; i++)
            {
                p1 = p1 + 0.2f;
                sw.Write(p1);
                sw.Write("\r\n");
                sw.Write("rtLeftCam.WorldToScreenPoint(aux)");
                sw.Write("\r\n");
                //for (i = 0; i < 4; i++)
                {
                    aux.Set(0.0f, 0.0f, p1);
                    sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                    sw.Write("\r\n");
                    aux.Set(0.0f, c1, p1);
                    sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                    sw.Write("\r\n");
                    aux.Set(0.0f, -c1, p1);
                    sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                    sw.Write("\r\n");
                    aux.Set(c1, 0.0f, p1);
                    sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                    sw.Write("\r\n");
                    aux.Set(-c1, 0.0f, p1);
                    sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                    sw.Write("\r\n");
                    aux.Set(c1, c1, p1);
                    sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                    sw.Write("\r\n");
                    aux.Set(-c1, -c1, p1);
                    sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                    sw.Write("\r\n");
                    aux.Set(c1, -c1, p1);
                    sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                    sw.Write("\r\n");
                    aux.Set(-c1, c1, p1);
                    sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                    sw.Write("\r\n");
                }
                sw.Write("rtRightCam.WorldToScreenPoint(aux)");
                sw.Write("\r\n");
                //for (i = 0; i < 4; i++)
                {
                    aux.Set(0.0f, 0.0f, p1);
                    sw.Write(rtRightCam.WorldToScreenPoint(aux));
                    sw.Write("\r\n");
                    aux.Set(0.0f, c1, p1);
                    sw.Write(rtRightCam.WorldToScreenPoint(aux));
                    sw.Write("\r\n");
                    aux.Set(0.0f, -c1, p1);
                    sw.Write(rtRightCam.WorldToScreenPoint(aux));
                    sw.Write("\r\n");
                    aux.Set(c1, 0.0f, p1);
                    sw.Write(rtRightCam.WorldToScreenPoint(aux));
                    sw.Write("\r\n");
                    aux.Set(-c1, 0.0f, p1);
                    sw.Write(rtRightCam.WorldToScreenPoint(aux));
                    sw.Write("\r\n");
                    aux.Set(c1, c1, p1);
                    sw.Write(rtRightCam.WorldToScreenPoint(aux));
                    sw.Write("\r\n");
                    aux.Set(-c1, -c1, p1);
                    sw.Write(rtRightCam.WorldToScreenPoint(aux));
                    sw.Write("\r\n");
                    aux.Set(c1, -c1, p1);
                    sw.Write(rtRightCam.WorldToScreenPoint(aux));
                    sw.Write("\r\n");
                    aux.Set(-c1, c1, p1);
                    sw.Write(rtRightCam.WorldToScreenPoint(aux));
                    sw.Write("\r\n");
                }
            }
            p1 = 1.0f;
            c1 = 0.0f;
            sw.Write("\r\n");
            sw.Write("\r\n");
            for (i = 0; i < 4; i++)
            {
                c1 = c1 + 0.1f;
                sw.Write(c1);
                sw.Write("\r\n");
                sw.Write("rtLeftCam.WorldToScreenPoint(aux)");
                sw.Write("\r\n");
                aux.Set(-c1, c1, p1);
                sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
                aux.Set(-c1, -c1, p1);
                sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
                aux.Set(c1, c1, p1);
                sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
                aux.Set(c1, -c1, p1);
                sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");

                sw.Write("rtRightCam.WorldToScreenPoint(aux)");
                sw.Write("\r\n");
                aux.Set(-c1, c1, p1);
                sw.Write(rtRightCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
                aux.Set(-c1, -c1, p1);
                sw.Write(rtRightCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
                aux.Set(c1, c1, p1);
                sw.Write(rtRightCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
                aux.Set(c1, -c1, p1);
                sw.Write(rtRightCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
            }


            p1 = 1.0f;
            c1 = 0.4f;
            float c2 = 0.4f;
            sw.Write("\r\n");
            sw.Write("\r\n");
            for (i = 0; i < 4; i++)
            {
                c2 = c2 + 0.1f;
                sw.Write(c2);
                sw.Write("\r\n");
                sw.Write("rtLeftCam.WorldToScreenPoint(aux)");
                sw.Write("\r\n");
                aux.Set(-c2, c1, p1);
                sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
                aux.Set(-c2, -c1, p1);
                sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
                aux.Set(c2, c1, p1);
                sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
                aux.Set(c2, -c1, p1);
                sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");

                sw.Write("rtRightCam.WorldToScreenPoint(aux)");
                sw.Write("\r\n");
                aux.Set(-c2, c1, p1);
                sw.Write(rtRightCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
                aux.Set(-c2, -c1, p1);
                sw.Write(rtRightCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
                aux.Set(c2, c1, p1);
                sw.Write(rtRightCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
                aux.Set(c2, -c1, p1);
                sw.Write(rtRightCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
            }

            p1 = 0.4f;
            c1 = 0.0f;
            sw.Write("\r\n");
            sw.Write("\r\n");
            sw.Write("Para 0.4");
            sw.Write("\r\n");
            sw.Write(p1);
            sw.Write("\r\n");
            for (i = 0; i < 2; i++)
            {
                c1 = c1 + 0.1f;
                sw.Write(c1);
                sw.Write("\r\n");
                sw.Write("rtLeftCam.WorldToScreenPoint(aux)");
                sw.Write("\r\n");
                aux.Set(-c1, c1, p1);
                sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
                aux.Set(-c1, -c1, p1);
                sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
                aux.Set(c1, c1, p1);
                sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
                aux.Set(c1, -c1, p1);
                sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");

                sw.Write("rtRightCam.WorldToScreenPoint(aux)");
                sw.Write("\r\n");
                aux.Set(-c1, c1, p1);
                sw.Write(rtRightCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
                aux.Set(-c1, -c1, p1);
                sw.Write(rtRightCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
                aux.Set(c1, c1, p1);
                sw.Write(rtRightCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
                aux.Set(c1, -c1, p1);
                sw.Write(rtRightCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
            }


            p1 = 0.4f;
            c1 = 0.2f;
            c2 = 0.2f;
            sw.Write("\r\n");
            sw.Write("\r\n");
            for (i = 0; i < 2; i++)
            {
                c2 = c2 + 0.1f;
                sw.Write(c2);
                sw.Write("\r\n");
                sw.Write("rtLeftCam.WorldToScreenPoint(aux)");
                sw.Write("\r\n");
                aux.Set(-c2, c1, p1);
                sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
                aux.Set(-c2, -c1, p1);
                sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
                aux.Set(c2, c1, p1);
                sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
                aux.Set(c2, -c1, p1);
                sw.Write(rtLeftCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");

                sw.Write("rtRightCam.WorldToScreenPoint(aux)");
                sw.Write("\r\n");
                aux.Set(-c2, c1, p1);
                sw.Write(rtRightCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
                aux.Set(-c2, -c1, p1);
                sw.Write(rtRightCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
                aux.Set(c2, c1, p1);
                sw.Write(rtRightCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
                aux.Set(c2, -c1, p1);
                sw.Write(rtRightCam.WorldToScreenPoint(aux));
                sw.Write("\r\n");
            }
            /*sw.Write("rtLeftCam.WorldToViewportPoint(aux)");
            sw.Write("\r\n");
            sw.Write(rtLeftCam.WorldToViewportPoint(aux));
            sw.Write("\r\n");

            sw.Write("rtLeftCam.sensorSize");
            sw.Write("\r\n");
            sw.Write(rtLeftCam.sensorSize);
            sw.Write("\r\n");
            sw.Write("rtLeftCam.transform.localRotation");
            sw.Write("\r\n");
            sw.Write(rtLeftCam.transform.localRotation);
            sw.Write("\r\n");

            sw.Write("this.eyeDepth");
            sw.Write("\r\n");
            sw.Write(this.eyeDepth);
            sw.Write("\r\n");*/
            sw.Close();
        }
    }

    public void Initialization(int plat, int track, bool capture)
    {
        editorCam = this.GetComponent<Camera>();
        platform = plat;
        tracking = track;
        fab_calib = false;

        if (platform == 0)
        {
            pcPlugin = new CalibrationData();
        }

        else if (platform != 0)
        {
            androidPlugin = new AndroidCalibration();
            androidPlugin.UpdateCalibration();
            if (platform == 1) Screen.orientation = ScreenOrientation.LandscapeRight;
            else if (platform > 1) Screen.orientation = ScreenOrientation.LandscapeLeft;
            
        }

        RigSettings();
        if(!capture) Destroy(editorCam);
        RTCameraPosition();
        CreateMeshes();

        if(track == 2) this.transform.localPosition = new Vector3(0.0f, -.05f, -.03f);
        else this.transform.localPosition = new Vector3(0.0f, -eyeHeight, -eyeDepth);
        Destroy(GetComponent<RigSetup>());
    }


    void RigSettings()
    {
        if (platform == 0)//Here is possible to change the ipd,fov,tilt and resolution of the camera
        {
            this.ipd = pcPlugin.IPD();
            this.rtResolutionPC = pcPlugin.RTRez();
            this.cameraTilt = pcPlugin.Tilt();
            this.fov = pcPlugin.FOV();

        }
        
        else 
        {
            this.ipd = androidPlugin.GetIpd();
            this.rtResolutionAndroid = androidPlugin.GetRtResolutionAndroid();
            this.cameraTilt = androidPlugin.GetCameraTilt();
            this.fov = androidPlugin.GetFov();
        }
    }

    void RTCameraPosition()
    {
        GameObject newCamLeft = new GameObject();
        newCamLeft.name = "LeftCamera";
        rtLeftCam = newCamLeft.AddComponent<Camera>();

        GameObject newCamRight = new GameObject();
        newCamRight.name = "RightCamera";
        rtRightCam = newCamRight.AddComponent<Camera>();

        rtLeftCam.enabled = true;
        rtRightCam.enabled = true;

        rtLeftCam.clearFlags = CameraClearFlags.SolidColor;
        rtLeftCam.backgroundColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);
        rtLeftCam.nearClipPlane = 0.1f;
        rtLeftCam.fieldOfView = 48;

        rtRightCam.clearFlags = CameraClearFlags.SolidColor;
        rtRightCam.backgroundColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);
        rtRightCam.nearClipPlane = 0.1f;
        rtRightCam.fieldOfView = 48;

        rtLeftCam.transform.SetParent(this.transform);
        rtRightCam.transform.SetParent(this.transform);

        if (tracking == 2)
        {
            rtLeftCam.transform.localPosition = new Vector3(-ipd / 2.0f, 0.0f, 0.0f);
            rtRightCam.transform.localPosition = new Vector3(ipd / 2.0f, 0.0f, 0.0f);
        }

        else
        {
            rtLeftCam.transform.localPosition = new Vector3(-ipd / 2.0f, eyeHeight, eyeDepth);
            rtRightCam.transform.localPosition = new Vector3(ipd / 2.0f, eyeHeight, eyeDepth);
        }
        //cameraTilt
        cameraRotation.eulerAngles = new Vector3(cameraTilt, 0.0f, 0.0f);//Camera tilt, Here you can change the camera tilt //cameraTilt
        rtLeftCam.transform.localRotation = cameraRotation;
        rtRightCam.transform.localRotation = cameraRotation;

        rtLeftCam.fieldOfView = this.fov;
        rtRightCam.fieldOfView = this.fov;

        if (platform == 0)
        {
            rtLeft = new RenderTexture((int)rtResolutionPC.x, (int)rtResolutionPC.y, 24, RenderTextureFormat.ARGB32);
            rtRight = new RenderTexture((int)rtResolutionPC.x, (int)rtResolutionPC.y, 24, RenderTextureFormat.ARGB32);
        }

        else if (platform != 0)
        {
            rtLeft = new RenderTexture((int)rtResolutionAndroid.x, (int)rtResolutionAndroid.y, 24, RenderTextureFormat.ARGB32);
            rtRight = new RenderTexture((int)rtResolutionAndroid.x, (int)rtResolutionAndroid.y, 24, RenderTextureFormat.ARGB32);
        }

        rtLeftCam.targetTexture = rtLeft;
        rtRightCam.targetTexture = rtRight;


        //Load Calibration Parameters
        read_int_ext(@"C:\Users\jpsfa\Documents\UC\DreamGlass_Project\DreamGlass\glasses_int_ext.json");
        load_calibration(setup_calib.Ext_l, setup_calib.Ext_r, setup_calib.Int_l, setup_calib.Int_r);
        if(fab_calib == false)
        {
            rtLeftCam.worldToCameraMatrix = setup_t_l;//Extrinsic parameters
            rtRightCam.worldToCameraMatrix = setup_t_r;//Extrinsic parameters
            rtLeftCam.projectionMatrix = setup_m_l;//Intrinsic parameters
            rtRightCam.projectionMatrix = setup_m_r;//Intrinsic parameters
        }

        //Verify updated parameters
        guarda();
    }

    public void CreateMeshes()
    {

        leftMeshMat = new Material(Shader.Find("Unlit/Texture"));
        rightMeshMat = new Material(Shader.Find("Unlit/Texture"));
        leftMeshMat.mainTexture = rtLeft;
        rightMeshMat.mainTexture = rtRight;

        GameObject leftEyeMesh = new GameObject();
        leftEyeMesh.name = "LeftEyeMesh";
        leftEyeMesh.transform.SetParent(this.transform);
        leftEyeMesh.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
        leftEyeMesh.AddComponent<Distortion>();

        GameObject rightEyeMesh = new GameObject();
        rightEyeMesh.name = "RightEyeMesh";
        rightEyeMesh.transform.SetParent(this.transform);
        rightEyeMesh.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
        rightEyeMesh.AddComponent<Distortion>();

        leftEyeMesh.GetComponent<Distortion>().BuildMesh(true, platform, leftMeshMat);
        rightEyeMesh.GetComponent<Distortion>().BuildMesh(false, platform, rightMeshMat);

    }

}
