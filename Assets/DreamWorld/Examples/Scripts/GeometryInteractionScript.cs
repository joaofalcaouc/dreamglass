﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
Script for object movement
    You can change:
    * Object position
    * Displayed text
    * Others
**/

public class GeometryInteractionScript : MonoBehaviour {

    public TextMesh interactionText; // Text to be displayed with the gesture
    public ParticleSystem particles; //particles displayed
    public Transform centerPos; // Position of the object
    public Transform returnPos; // Inicial position of the object
    private Transform focusCenter; // 
    private Renderer geometryRend;
    private Vector3 newPos;
    private bool holding;
    private Color col;
    private bool initilized; 

    void InitializeGeometry()
    {  
        this.focusCenter = DWCameraRig.Instance.GetFocusCenter();

        if (this.centerPos != null)
        {
            this.centerPos.position = focusCenter.position;
            this.centerPos.rotation = focusCenter.rotation;
          //  this.centerPos.SetParent(DWCameraRig.Instance.transform);
            this.centerPos.position += centerPos.transform.forward * 0.7f;
        }

        if (this.returnPos != null)
        {
            this.returnPos.position = focusCenter.position;
            this.returnPos.rotation = focusCenter.rotation;
           // this.returnPos.SetParent(DWCameraRig.Instance.transform);
            this.returnPos.position += returnPos.transform.forward * 1.1f;
        }

        newPos = this.transform.position;//Stating position of the object
        geometryRend = this.GetComponent<Renderer>();//Virtual information of the object to be rendered: position,... to show the object
        //col = Color.red;
        //geometryRend.material.color = col;
        //if (particles != null) particles.startColor = col;
        initilized = true;
    }

    public void FocusOn() //Object focused
    {

       interactionText.text = "FocusOn";
    }

    public void FocusOff() //Object not on cursor focus
    {
        interactionText.text = "FocusOff";
    }

    public void Clicked()//Starts the particles movement
    {
       if(this.particles != null) particles.Play();
       interactionText.text = "Clicked";
    }

    public void OpenPalmed()//Open Palmed changes the color of the object
    {
        //if (col == Color.red) col = Color.green;
        //else if (col == Color.green) col = Color.blue;
        //else if (col == Color.blue) col = Color.red;

        //particles.startColor = col;
        //geometryRend.material.color = col;

        interactionText.text = "OpenPalm";
    }

    public void Held()
    {
        interactionText.text = "Holding";
        holding = true;//Object held
    }

    public void LetGo()
    {
        interactionText.text = "Released";
        newPos = returnPos.position;//Update new position of object
        holding = false;//Stops holding
    }

    public void MoveGeometry()
    {
        if (holding)//Moves object according to cursor movement
        {

            //this.transform.position = Vector3.Lerp(this.transform.position, centerPos.position, Time.deltaTime * 15.0f);
        }

        else//Relative position when cursor is moved without holding
        {
            //this.transform.position = Vector3.Lerp(this.transform.position, newPos, Time.deltaTime * 15.0f);
        }
    }

    void Update()
    {
        if (!initilized && DWCameraRig.Instance.GetFocusCenter() != null) InitializeGeometry();//Initialize
        if(initilized) MoveGeometry();//Movement of objects
    }
}
