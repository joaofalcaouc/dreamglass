﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/**
 * Available Gestures and interactions
 * Gestures:Click,Open Palm, Hold/UnHold
 * Interactions: Focus and Unfocus between Object and Cursor
 * 
 * Public variables in script can be connected with other scripts thats why is possible to connect all the scripts in unity ui
 **/
public class HandGestureInteraction : MonoBehaviour {

    public UnityEvent focusEvent;
    public UnityEvent unfocusEvent;
    public UnityEvent onClickEvent;
    public UnityEvent onOpenPalmEvent;
    public UnityEvent onHoldEvent;
    public UnityEvent onReleaseEvent;

    //public UnityEvent onSelectEvent;

    void OnFocus()
    {
        if (this.enabled == false) return;
        if (focusEvent != null)
        {
            focusEvent.Invoke();
        }
    }

    void Unfocus()
    {
        if (this.enabled == false) return;
        if (unfocusEvent != null)
        {
            unfocusEvent.Invoke();
        }
    }

    void OnClick()
    {
        if (this.enabled == false) return;
        if (onClickEvent != null)
        {
            onClickEvent.Invoke();
        }
    }

    void OnOpenPalm()
    {
        if (this.enabled == false) return;
        if (onOpenPalmEvent != null)
        {
            onOpenPalmEvent.Invoke();
        }
    }

    void OnHold()
    {
        if (this.enabled == false) return;
        if (onHoldEvent != null)
        {
            onHoldEvent.Invoke();
        }
    }

    void OnRelease()
    {

        if (this.enabled == false) return;
        if (onReleaseEvent != null)
        {
            onReleaseEvent.Invoke();
        }
    }
}
