﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class second_screen : MonoBehaviour
{
    public GameObject tex_obj;
    public GameObject Parent_Obj;
    private Texture2D tex_2;
    private Vector3 change_scale;
    int width, height;
    public int screen;
    // Start is called before the first frame update
    void Start()
    {
        width = tex_obj.GetComponent<display_mov>().width;//640,480//1920,1080//1366, 768
        height = tex_obj.GetComponent<display_mov>().height;
        tex_2 = new Texture2D(width, height, TextureFormat.RGB24, false);
        //tex_2 = (Texture2D)tex_obj.GetComponent<RawImage>().texture;
        this.GetComponent<RawImage>().texture = tex_2;

        if (screen == 0)
        {
            change_scale = transform.localScale + Vector3.one * 1.9f;
            transform.localScale = change_scale;
            var limit_x = (Parent_Obj.GetComponent<RectTransform>().rect.width / 2) / (this.GetComponent<RawImage>().rectTransform.rect.width * transform.localScale.x);
            var limit_y = (Parent_Obj.GetComponent<RectTransform>().rect.height / 2) / (this.GetComponent<RawImage>().rectTransform.rect.height * transform.localScale.y);
            change_pivot(1.0f - limit_x, 1.0f - limit_y);
        }
        else if (screen == 1)
        {
            change_scale = transform.localScale + Vector3.one * 14.0f;
            transform.localScale = change_scale;
            var limit_x = (Parent_Obj.GetComponent<RectTransform>().rect.width / 2) / (this.GetComponent<RawImage>().rectTransform.rect.width * transform.localScale.x);
            var limit_y = (Parent_Obj.GetComponent<RectTransform>().rect.height / 2) / (this.GetComponent<RawImage>().rectTransform.rect.height * transform.localScale.y);
            change_pivot(1.0f - limit_x - 0.047f, limit_y + 0.014f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //tex_2 = (Texture2D)tex_obj.GetComponent<RawImage>().texture;
        Graphics.CopyTexture((Texture2D)tex_obj.GetComponent<RawImage>().texture, tex_2);
        tex_2.Apply();
    }

    void change_pivot(float var_x, float var_y)//Change Pivot according to zoom
    {
        Vector2 pivot_value;
        var limit_x = (Parent_Obj.GetComponent<RectTransform>().rect.width / 2) / (this.GetComponent<RawImage>().rectTransform.rect.width * transform.localScale.x);
        var limit_y = (Parent_Obj.GetComponent<RectTransform>().rect.height / 2) / (this.GetComponent<RawImage>().rectTransform.rect.height * transform.localScale.y);
        pivot_value = this.GetComponent<RawImage>().rectTransform.pivot;
        if (var_x >= limit_x + (284.0f / 1920.0f) && var_x <= (1 - limit_x))//&& dual_screen == false)//4.5f/31.0f
        {
            pivot_value.x = var_x;// pivot value needs to be between (0,0) to (1,1)
        }
        else
        {
            //pivot_value.x = this.GetComponent<RawImage>().rectTransform.pivot.x;
            if (transform.localScale.x > 1.0f)
            {
                if (var_x < limit_x + (284.0f / 1920.0f))
                {
                    pivot_value.x = limit_x + (284.0f / 1920.0f);
                }
                else
                {
                    pivot_value.x = 1.0f - limit_x;
                }
            }
            else
            {
                pivot_value.x = this.GetComponent<RawImage>().rectTransform.pivot.x;
            }

        }

        if (var_y >= limit_y && var_y <= (1 - limit_y))//&& dual_screen == false)
        {
            pivot_value.y = var_y;
        }
        else
        {
            //pivot_value.y = this.GetComponent<RawImage>().rectTransform.pivot.y;
            if (transform.localScale.y > 1.0f)
            {
                if (var_y < limit_y + (80.0f / 1080.0f))
                {
                    pivot_value.y = limit_y + (80.0f / 1080.0f);
                }
                else
                {
                    pivot_value.y = 1.0f - limit_y;
                }
            }
            else
            {
                pivot_value.y = this.GetComponent<RawImage>().rectTransform.pivot.y;
            }
        }
        this.GetComponent<RawImage>().rectTransform.pivot = pivot_value;//Change position of the zoom
    }
}
