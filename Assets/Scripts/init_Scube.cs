﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class init_Scube : MonoBehaviour
{
    public GameObject cube;
    public GameObject[] spheres;
    float l;
    // Start is called before the first frame update
    void Start()
    {
        l = cube.transform.localScale.x;
        Vector3 aux;
        for(int i = 0; i < 8; i++)
        {
            aux = spheres[i].transform.localPosition;
            //____
            switch (i)
            {
                case 0:
                    aux.x = l / 2.0f;
                    aux.y = l / 2.0f;
                    aux.z = l / 2.0f;
                    break;
                case 1:
                    aux.x = -l / 2.0f;
                    aux.y = l / 2.0f;
                    aux.z = l / 2.0f;
                    break;
                case 2:
                    aux.x = l / 2.0f;
                    aux.y = -l / 2.0f;
                    aux.z = l / 2.0f;
                    break;
                case 3:
                    aux.x = -l / 2.0f;
                    aux.y = -l / 2.0f;
                    aux.z = l / 2.0f;
                    break;
                case 4:
                    aux.x = l / 2.0f;
                    aux.y = l / 2.0f;
                    aux.z = -l / 2.0f;
                    break;
                case 5:
                    aux.x = -l / 2.0f;
                    aux.y = l / 2.0f;
                    aux.z = -l / 2.0f;
                    break;
                case 6:
                    aux.x = l / 2.0f;
                    aux.y = -l / 2.0f;
                    aux.z = -l / 2.0f;
                    break;
                case 7:
                    aux.x = -l / 2.0f;
                    aux.y = -l / 2.0f;
                    aux.z = -l / 2.0f;
                    break;
                default:
                    aux.x = 0.0f;
                    aux.y = 0.0f;
                    aux.z = 0.0f;
                    break;
            }
            //____
            spheres[i].transform.localPosition = aux;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
