﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

using System.Runtime.InteropServices;

[Serializable]
class calib_data
{
    public int width;
    public int heigth;
    public string frame_file;
    public List<calibrations> calibrations;
};
[Serializable]
class calibrations
{
    public K K;
    public string dist_model;
    public dist dist;
};
[Serializable]
class K
{
    public int rows;
    public int cols;
    public string dt;
    public double[] data;
};
[Serializable]
class dist
{
    public int rows;
    public int cols;
    public string dt;
    public double[] data;
};

[Serializable]
class registration_T
{
    public int rows;
    public int cols;
    public string dt;
    public double[] point;
    public double[] data;
};

[Serializable]
class glasses_calib
{
    public int rows;
    public int cols;
    public string dt;
    public double[] data;
    public double[] P_cube;
    public double[] S;
    public double[] Ext_l;
    public double[] Ext_r;
    public double[] Int_l;
    public double[] Int_r;
};

//--------------------------------------

public class tcpconnect : MonoBehaviour
{
    [DllImport("Eigen_Integration.dll", CallingConvention = CallingConvention.Cdecl)]//, CharSet = CharSet.Unicode
    static extern unsafe void bouguet_method(double[] T_u, double[] K_u, double[] Kc_u, int l, [Out] double[] result);
    [DllImport("Eigen_Integration.dll", CallingConvention = CallingConvention.Cdecl)]
    static extern unsafe void calib_method(double[] T_u, double[] T_a, [Out] double[] result);
    [DllImport("Eigen_Integration.dll", CallingConvention = CallingConvention.Cdecl)]
    static extern unsafe void point_method(double[] T_u, double[] P_a, double[] T_o, double[] K_u, double[] Kc_u, [Out] double[] result);
    [DllImport("Eigen_Integration.dll", CallingConvention = CallingConvention.Cdecl)]
    static extern unsafe void model_method(double[] S_a,double[] T_u, double[] T_a, double[] T_o, [Out] double[] trans, [Out] double[] rot);
    [DllImport("Eigen_Integration.dll", CallingConvention = CallingConvention.Cdecl)]
    static extern unsafe void multisphere_method(double[] T_calib, double[] S_a, double[] T_a, double[] Pcube, [Out] double[] posit);
    [DllImport("Eigen_Integration.dll", CallingConvention = CallingConvention.Cdecl)]
    static extern unsafe void spaam_method(double[] S_a, double[] T_a, double[] Pcube, [Out] double[] posit);
    [DllImport("Eigen_Integration.dll", CallingConvention = CallingConvention.Cdecl)]
    static extern unsafe void calib_method_verification(double[] S_a, double[] T_a, [Out] double[] posit, [Out] double[] rotit);

    //These are the variables for the TCP connections
    //There could be only one variable but in order to understand each connection, four variables were created for each type (3 in total)
    private TcpListener tcpipList, tcppose, tcpkeyboard, tcpguide;
    private Thread tcpThread_1, tcpThread_2, tcpThread_3, tcpThread_4;
    private TcpClient tcpipClient, tcpClientpose, tcpClientkeyboard, tcpClientguide;

    public int width, height;

    //Variables for data sharing between threads
    //Public variables can be acessed in other modules
    //[HideInInspector] is just to hide the variable form unity interface
    [HideInInspector]
    public Byte[] video_bytes;
    static Byte[] bytes_video;//Raw Video
    static Byte[] bytes_pose;//Raw 3dpose
    static Byte[] bytes_keyboard;//Raw beyboard byte
    [HideInInspector]
    public Byte[] bytes_guide;
    [HideInInspector]
    public Byte keyboard_key;
    //static Byte[] bytes_model;//model
    //---------------------

    //3D Pose of marker, Calibration data
    double[] pose_3d_1;// Will be used as T in the bouguet and Barreto Methods
    double[] pose_3d_2;
    double[] pose_3d_3;
    double[] pose_3d_4;
    calib_data calibration_data;// K and Kc are there
    registration_T registration;
    glasses_calib calibration_glasses;

    [HideInInspector]
    public double[] poi_cube_1;//Marker vertices position and center in the in.nav display
    [HideInInspector]
    public double[] poi_cube_2;
    [HideInInspector]
    public double[] poi_cube_3;
    [HideInInspector]
    public double[] poi_cube_4;
    //----------------

    //Variable for confirmation that video is being received (kind off)
    static int upvideo;
    [HideInInspector]
    public bool data_ready;

    private int l;
    public GameObject screen_control;

    //Canvas movement
    public GameObject plano;

    [HideInInspector]
    public double[] position;
    [HideInInspector]
    public double[] rotation;
    [HideInInspector]
    public double[] guide_data;

    //---------------
    public GameObject model;
    public bool Mode_1_on;
    public bool Calib_Glasses;

    public GameObject calibration;
    public GameObject Camera_DG;

    [HideInInspector]
    public double[] sphere_position;
    [HideInInspector]
    public double[] marker_position;
    [HideInInspector]
    public double[] marker_rotation;

    /// <summary>
    /// Function runned in one of the threads that receives data
    /// The port used is given by port
    /// All the arrays used are dependent on the size, so in order to receive in a different size just change
    /// the number of bytes of this array defined in Start()
    /// </summary>
    private void tcp_data_feed(int port, TcpListener listener, TcpClient client, byte[] all_data)
    {
        //string serverMessage = "This is a message from your server 1";
        //byte[] serverMessageAsByteArray = Encoding.ASCII.GetBytes(serverMessage);
        int tamanho = 0;
        int length_data;
        Byte[] incdata;
        Byte[] incdata_2;
        incdata = new Byte[all_data.Length];
        incdata_2 = new Byte[all_data.Length];
        try
        {
            // Create listener on localhost port 12500 			
            listener = new TcpListener(IPAddress.Parse("127.0.0.1"), port);
            listener.Start();
            Debug.Log(string.Format("Server in port {0} is listening", port));
            while (true)
            {
                using (client = listener.AcceptTcpClient())
                {
                    Debug.Log(string.Format("Client recv in server ({0})",port));
                    // Get a stream object for reading 				
                    upvideo = 1;
                    using (NetworkStream stream = client.GetStream())
                    {
                        // Read incomming stream into byte arrary. 						
                        while ((length_data = stream.Read(incdata, 0, all_data.Length - tamanho)) != 0)
                        {
                            Buffer.BlockCopy(incdata, 0, incdata_2, tamanho, length_data);
                            tamanho = tamanho + length_data;
                            if (tamanho == all_data.Length)
                            {
                                Buffer.BlockCopy(incdata_2, 0, all_data, 0, all_data.Length);
                                //stream.Write(serverMessageAsByteArray, 0, serverMessageAsByteArray.Length);
                                tamanho = 0;
                            }
                        }
                    }
                }

            }
        }
        catch (SocketException socketException)
        {
            Debug.Log("SocketException " + socketException.ToString());
            upvideo = 0;
        }
    }

    void BytetoDouble(double[] darray, Byte[] barray, int offset)
    {
        //First way
        /*int i = 0;
        for (i = 0; i < darray.Length; i++)
        {
            darray[i] = BitConverter.ToDouble(barray, i * sizeof(double));
        }*/

        //Second way
        Buffer.BlockCopy(barray, offset * sizeof(double), darray, 0, darray.Length * sizeof(double));
    }

    /// <summary>
    /// Function to read all the calibration data available. The file readed is a JSON file so any YAML files need to be converted.
    /// On the site:https://www.json2yaml.com/convert-yaml-to-json is possible to convert the file without much work.
    /// The stored data stays in the variable calibration_data.
    /// </summary>
    /// <param name="path_file"></param>
    void read_calibration(string path_file)
    {
        if (File.Exists(path_file))
        {
            string jsonddata = File.ReadAllText(path_file);
            calibration_data = JsonUtility.FromJson<calib_data>(jsonddata);
        }
        else
        {
            Debug.LogError("Cannot open file for calibration data");
        }
    }

    void read_model_data(string path_file)
    {
        if (File.Exists(path_file))
        {
            string jsonddata = File.ReadAllText(path_file);
            registration = JsonUtility.FromJson<registration_T>(jsonddata);
        }
        else
        {
            Debug.LogError("Cannot open file for calibration data");
        }
    }

    void read_calib_glasses(string path_file)
    {
        if (File.Exists(path_file))
        {
            string jsonddata = File.ReadAllText(path_file);
            calibration_glasses = JsonUtility.FromJson<glasses_calib>(jsonddata);
        }
        else
        {
            Debug.LogError("Cannot open file for calibration data");
        }
    }

    // Use this for initialization
    void Start()
    {
        //640,480//1920,1080//1366, 768
        //Mode_1_on = false;
        //Definition of all the variables used for reading the video/pose bytes from the tcp/ip connection
        bytes_video = new Byte[width * height * 3];//480*640*4//1080 * 1920 * 4 //1366 * 768 * 4
        bytes_pose = new Byte[128 * 4];
        bytes_keyboard = new Byte[1];
        bytes_guide = new Byte[24];
        //----------------------------------------------------------------------

        calibration.SetActive(true);

        //Definition of extra necessary variables for calibration and read form file
        calibration_data = new calib_data();
        read_calibration(@"C:\repo\test\innav_apps\workspace_tsa\calibrations\cameracalib_last.json");
        read_model_data(@"C:\repo\test\innav_apps\workspace_tsa\data\shoulder_registration_last.json");
        read_calib_glasses(@"C:\Users\jpsfa\Documents\UC\DreamGlass_Project\DreamGlass\glasses_calib_last.json");
        //----------------------------------------------------------------------

        poi_cube_1 = new double[10];
        poi_cube_2 = new double[10];
        poi_cube_3 = new double[10];
        poi_cube_4 = new double[10];
        l = 15;

        //Confirmation of the video
        upvideo = 0;
        data_ready = false;//Data is not being received

        //3d Pose of the markers
        pose_3d_1 = new double[16];
        pose_3d_2 = new double[16];
        pose_3d_3 = new double[16];
        pose_3d_4 = new double[16];
        //-------------------------

        guide_data = new double[3];

        //Run Tcp/ip connection in other thread because of interrupts(can't happen on main thread or the program will crash)

        //Receive Video
        tcpThread_1 = new Thread(() => tcp_data_feed(12500, tcpipList, tcpipClient, bytes_video));
        tcpThread_1.IsBackground = true;
        tcpThread_1.Start();

        //Receive Marker Poses
        tcpThread_2 = new Thread(() => tcp_data_feed(12501, tcppose, tcpClientpose, bytes_pose));
        tcpThread_2.IsBackground = true;
        tcpThread_2.Start();

        //Receive Keyboard Key 
        tcpThread_3 = new Thread(() => tcp_data_feed(12502, tcpkeyboard, tcpClientkeyboard, bytes_keyboard));
        tcpThread_3.IsBackground = true;
        tcpThread_3.Start();

        tcpThread_4 = new Thread(() => tcp_data_feed(12503, tcpguide, tcpClientguide, bytes_guide));
        tcpThread_4.IsBackground = true;
        tcpThread_4.Start();
        //--------------------------------------
    }

    // Update is called once per frame
    void Update()
    {
        if (upvideo > 0)
        {
            data_ready = true;
            //Debug.Log("Total Memory: " + GC.GetTotalMemory(false));

            pose_3d_1 = new double[16];
            pose_3d_2 = new double[16];
            pose_3d_3 = new double[16];
            pose_3d_4 = new double[16];
            BytetoDouble(pose_3d_1, bytes_pose, 0);
            BytetoDouble(pose_3d_2, bytes_pose, 16);
            BytetoDouble(pose_3d_3, bytes_pose, 32);
            BytetoDouble(pose_3d_4, bytes_pose, 48);
            BytetoDouble(guide_data, bytes_guide, 0);
            //Debug.Log(Time.realtimeSinceStartup);

            //---------------
            if (Mode_1_on)
            {
                model.SetActive(false);
                plano.SetActive(true);
                calibration.SetActive(false);

                poi_cube_1 = new double[10];
                poi_cube_2 = new double[10];
                poi_cube_3 = new double[10];
                poi_cube_4 = new double[10];

                if (screen_control.GetComponent<display_mov>().crop_marker_model)
                {
                    point_method(pose_3d_1, registration.point, registration.data, calibration_data.calibrations[1].K.data, calibration_data.calibrations[1].dist.data, poi_cube_1);
                    point_method(pose_3d_2, registration.point, registration.data, calibration_data.calibrations[1].K.data, calibration_data.calibrations[1].dist.data, poi_cube_2);
                    point_method(pose_3d_3, registration.point, registration.data, calibration_data.calibrations[1].K.data, calibration_data.calibrations[1].dist.data, poi_cube_3);
                    point_method(pose_3d_4, registration.point, registration.data, calibration_data.calibrations[1].K.data, calibration_data.calibrations[1].dist.data, poi_cube_4);
                }
                else
                {
                    bouguet_method(pose_3d_1, calibration_data.calibrations[1].K.data, calibration_data.calibrations[1].dist.data, l, poi_cube_1);
                    bouguet_method(pose_3d_2, calibration_data.calibrations[1].K.data, calibration_data.calibrations[1].dist.data, l, poi_cube_2);
                    bouguet_method(pose_3d_3, calibration_data.calibrations[1].K.data, calibration_data.calibrations[1].dist.data, l, poi_cube_3);
                    bouguet_method(pose_3d_4, calibration_data.calibrations[1].K.data, calibration_data.calibrations[1].dist.data, l, poi_cube_4);
                }
                position = new double[3];
                calib_method(pose_3d_1, calibration_glasses.data, position);
                video_bytes = bytes_video;
            }
            else
            {
                
                if (Calib_Glasses)//For calibration (incomplete process)
                {
                    model.SetActive(false);
                    plano.SetActive(false);
                    calibration.SetActive(true); 
                }
                else// See model
                {
                    model.SetActive(true);
                    plano.SetActive(false);
                    calibration.SetActive(false);

                    //sphere_position = new double[8*4];

                    //multisphere_method(calibration_glasses.data,calibration_glasses.S, pose_3d_1, calibration_glasses.P_cube, sphere_position);
                    //spaam_method(calibration_glasses.S, pose_3d_1, calibration_glasses.P_cube, sphere_position);

                    marker_position = new double[3];
                    marker_rotation = new double[23];
                    calib_method_verification(calibration_glasses.S, pose_3d_1, marker_position, marker_rotation);

                    position = new double[3];
                    rotation = new double[23];
                    model_method(calibration_glasses.S,pose_3d_1, calibration_glasses.data, registration.data, position, rotation);
                    /*Debug.Log(rotation[0]);
                    Debug.Log(rotation[1]);
                    Debug.Log(rotation[2]);
                    Debug.Log("----");*/
                    //Debug.Log(guide_data[0]);
                    //Debug.Log(guide_data[1]);
                    //Debug.Log(guide_data[2]);
                }
            }

            //Update Key Pressed
            keyboard_key = bytes_keyboard[0];


            if (bytes_keyboard[0] == 81)// Press Q to quit
            {
                Application.Quit();
            }


            //---------------
        }
        GC.Collect();
    }
}