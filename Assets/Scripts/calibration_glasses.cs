﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class calibration_glasses : MonoBehaviour
{
    public GameObject cube;
    public GameObject sphere;
    public GameObject parent_obj;
    Vector3 newposition;
    Vector3[] vertices;
    int i;
    // Start is called before the first frame update
    void Start()
    {
        i = 0;
        vertices = cube.GetComponent<MeshFilter>().mesh.vertices;
        Debug.Log(transform.TransformPoint(vertices[i]).x);
        Debug.Log(transform.TransformPoint(vertices[i]).y);
        Debug.Log(transform.TransformPoint(vertices[i]).z);
        Debug.Log("----------------------------");
        i++;
        Debug.Log(transform.TransformPoint(vertices[i]).x);
        Debug.Log(transform.TransformPoint(vertices[i]).y);
        Debug.Log(transform.TransformPoint(vertices[i]).z);
        Debug.Log("----------------------------");
        i++;
        Debug.Log(transform.TransformPoint(vertices[i]).x);
        Debug.Log(transform.TransformPoint(vertices[i]).y);
        Debug.Log(transform.TransformPoint(vertices[i]).z);
        Debug.Log("----------------------------");
        i++;
        Debug.Log(transform.TransformPoint(vertices[i]).x);
        Debug.Log(transform.TransformPoint(vertices[i]).y);
        Debug.Log(transform.TransformPoint(vertices[i]).z);


        using (StreamWriter sw = new StreamWriter("Cam_calib.txt", false))//Path.Combine(mydocpath, "TestFile.txt")
        {
            float aux;
            for (int i = 0; i < 8; i++)
            {
                sw.Write(transform.TransformPoint(vertices[i]).x);
                sw.Write(" , ");
                sw.Write(transform.TransformPoint(vertices[i]).y);
                sw.Write(" , ");
                sw.Write(transform.TransformPoint(vertices[i]).z);
                sw.Write("\r\n");
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        i = 0;
        vertices = cube.GetComponent<MeshFilter>().mesh.vertices;
        newposition = sphere.transform.localPosition;
        newposition.x = transform.TransformPoint(vertices[i]).x - parent_obj.transform.localPosition.x;
        newposition.y = transform.TransformPoint(vertices[i]).y - parent_obj.transform.localPosition.y;
        newposition.z = transform.TransformPoint(vertices[i]).z - parent_obj.transform.localPosition.z;
        sphere.transform.localPosition = newposition;
        /*if (i == 8)
        {
            i = 0;
        }
        else
        {
            i++;
        }*/


        Vector3 auxi;
        if(Input.GetKeyDown(KeyCode.UpArrow))
        {
            cube.transform.Translate(Vector3.up * Time.deltaTime, Space.World);
        }
        else if(Input.GetKeyDown(KeyCode.DownArrow))
        {
            cube.transform.Translate(Vector3.down * Time.deltaTime, Space.World);
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            cube.transform.Translate(Vector3.left * Time.deltaTime, Space.World);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            cube.transform.Translate(Vector3.right * Time.deltaTime, Space.World);
        }
        else if (Input.GetKeyDown(KeyCode.W))
        {
            cube.transform.Translate(Vector3.forward * Time.deltaTime, Space.World);
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            cube.transform.Translate(Vector3.back * Time.deltaTime, Space.World);
        }


        using (StreamWriter sw = new StreamWriter("Cam_calib.txt", false))//Path.Combine(mydocpath, "TestFile.txt")
        {
            float aux;
            for (int i = 0; i < 8; i++)
            {
                sw.Write(transform.TransformPoint(vertices[i]).x);
                sw.Write(" , ");
                sw.Write(transform.TransformPoint(vertices[i]).y);
                sw.Write(" , ");
                sw.Write(transform.TransformPoint(vertices[i]).z);
                sw.Write("\r\n");
            }
        }
    }
}
