﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class display_mov : MonoBehaviour
{
    private Texture2D tex;// Texture to be applied to canvas
    private bool two_marker_mode;
    private bool single_key;
    private bool dual_screen;
    private bool base_lock;
    private float zoom = 0.25f;//0.5f
    [HideInInspector]
    public bool crop_marker_model;
    double[] position;
    private Vector3 change_scale;
    Vector3 centerforplane;
    double[] poi_cube_1;
    double[] poi_cube_2;
    double[] poi_cube_3;
    double[] poi_cube_4;
    private float poi_x, poi_y, poi_x_inc, poi_y_inc;
    float x, y, z;
    public int width,height;

    public GameObject screen_1;
    public GameObject screen_2;
    public GameObject screen_3;
    public GameObject plano;
    public GameObject mainscript;

    private Byte keyboard_code;
    private Byte[] bytes_video;

    void change_pivot(float var_x, float var_y)//Change Pivot according to zoom
    {
        Vector2 pivot_value;
        var limit_x = (screen_1.GetComponent<RectTransform>().rect.width / 2) / (this.GetComponent<RawImage>().rectTransform.rect.width * transform.localScale.x);
        var limit_y = (screen_1.GetComponent<RectTransform>().rect.height / 2) / (this.GetComponent<RawImage>().rectTransform.rect.height * transform.localScale.y);
        pivot_value = this.GetComponent<RawImage>().rectTransform.pivot;
        if (var_x >= limit_x + (284.0f / 1920.0f) && var_x <= (1 - limit_x))//&& dual_screen == false)//4.5f/31.0f
        {
            pivot_value.x = var_x;// pivot value needs to be between (0,0) to (1,1)
        }
        else
        {
            //pivot_value.x = this.GetComponent<RawImage>().rectTransform.pivot.x;
            if (transform.localScale.x > 1.0f)
            {
                if (var_x < limit_x + (284.0f / 1920.0f))
                {
                    pivot_value.x = limit_x + (284.0f / 1920.0f);
                }
                else
                {
                    pivot_value.x = 1.0f - limit_x;
                }
            }
            else
            {
                pivot_value.x = this.GetComponent<RawImage>().rectTransform.pivot.x;
            }

        }

        if (var_y >= limit_y + (80.0f / 1080.0f) && var_y <= (1 - limit_y))//&& dual_screen == false)
        {
            pivot_value.y = var_y;
        }
        else
        {
            //pivot_value.y = this.GetComponent<RawImage>().rectTransform.pivot.y;
            if (transform.localScale.y > 1.0f)
            {
                if (var_y < limit_y + (80.0f / 1080.0f))
                {
                    pivot_value.y = limit_y + (80.0f / 1080.0f);
                }
                else
                {
                    pivot_value.y = 1.0f - limit_y;
                }
            }
            else
            {
                pivot_value.y = this.GetComponent<RawImage>().rectTransform.pivot.y;
            }
        }
        this.GetComponent<RawImage>().rectTransform.pivot = pivot_value;//Change position of the zoom
    }

    /// <summary>
    /// Function to update the center of focus of the crop and zoom in/out. For the zoom is used the keys I and O to zoom in and out, respectively.
    /// 
    /// </summary>
    void update_crop()
    {
        Vector2 pivot_value;
        pivot_value = this.GetComponent<RawImage>().rectTransform.pivot;
        pivot_value.x = 0.5f;
        pivot_value.y = 0.5f;
        //Zoom option
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        if (keyboard_code == 86 && single_key == false)//|| Input.GetKeyDown(KeyCode.I))
        {
            if (transform.localScale.x < Vector3.one.x * zoom * 4.0f + 1.0f)
            {
                change_scale = transform.localScale + Vector3.one * zoom;
                transform.localScale = change_scale;//Zoom IN
            }
            this.GetComponent<RawImage>().rectTransform.pivot = pivot_value;
            change_pivot(0.55f, 0.5f);
            single_key = true;
        }
        else if (keyboard_code == 66 && single_key == false)//|| Input.GetKeyDown(KeyCode.O))
        {
            if (transform.localScale.x >= Vector3.one.x * zoom + 1.0f)
            {
                change_scale = transform.localScale - Vector3.one * zoom;
                transform.localScale = change_scale;//Zoom OUT
            }
            this.GetComponent<RawImage>().rectTransform.pivot = pivot_value;
            if (transform.localScale.x > 1.0f)
            {
                change_pivot(0.55f, 0.5f);
            }
            single_key = true;
        }
        else if (keyboard_code == 48)
        {
            single_key = false;
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        //Two marker Option
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        if (keyboard_code == 78 && single_key == false)
        {
            two_marker_mode = true;
            single_key = true;
        }
        else if (keyboard_code == 77 && single_key == false)
        {
            two_marker_mode = false;
            single_key = true;
            if (crop_marker_model)
            {
                crop_marker_model = false;
            }
            else
            {
                crop_marker_model = true;
            }
        }
        else if (keyboard_code == 48)
        {
            single_key = false;
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        //Dual Screen Option
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        if (keyboard_code == 90 && single_key == false)//Z
        {
            dual_screen = true;
            screen_2.SetActive(true);
            Vector3 aux = screen_1.GetComponent<RectTransform>().localScale;
            aux.x = 1.0f;
            screen_1.GetComponent<RectTransform>().localScale = aux;

            aux = screen_1.GetComponent<RectTransform>().localPosition;
            aux.x = -480.0f;
            screen_1.GetComponent<RectTransform>().localPosition = aux;

            //Plane of canvas
            RectTransform auxi = screen_1.GetComponent<RectTransform>();
            auxi.sizeDelta = new Vector2(1920.0f / 2.0f, 1080.0f);
            screen_1.GetComponent<RectTransform>().sizeDelta = auxi.sizeDelta;
            single_key = true;
        }
        else if (keyboard_code == 88 && single_key == false)//X
        {
            dual_screen = false;
            screen_2.SetActive(false);
            Vector3 aux = screen_1.GetComponent<RectTransform>().localScale;
            aux.x = 1.0f;
            screen_1.GetComponent<RectTransform>().localScale = aux;

            aux = screen_1.GetComponent<RectTransform>().localPosition;
            aux.x = 0.0f;
            screen_1.GetComponent<RectTransform>().localPosition = aux;

            //Plane of canvas
            change_pivot(0.5f, 0.5f);//Possible Solution
            RectTransform auxi = screen_1.GetComponent<RectTransform>();
            auxi.sizeDelta = new Vector2(1920.0f, 1080.0f);
            screen_1.GetComponent<RectTransform>().sizeDelta = auxi.sizeDelta;
            single_key = true;
        }
        else if (keyboard_code == 48)
        {
            single_key = false;
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        if (keyboard_code == 75 && single_key == false)
        {
            if (base_lock)
            {
                base_lock = false;
                Vector3 aux = plano.transform.localPosition;
                aux.x = 0.0f;
                aux.y = 0.0f;
                aux.z = 0.6f;
                plano.transform.localPosition = aux;
                Quaternion auxi = plano.transform.localRotation;
                auxi.SetEulerAngles(0.0f, 0.0f, 0.0f);
                plano.transform.localRotation = auxi;
            }
            else
            {
                base_lock = true;
                Quaternion auxi = plano.transform.localRotation;
                auxi.SetEulerAngles(0.0f, 0.0f, 0.0f);
                plano.transform.localRotation = auxi;
            }
            single_key = true;
        }
        else if (keyboard_code == 48)
        {
            single_key = false;
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        //Change pivot and local position
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        if (two_marker_mode == true)
        {
            if (poi_cube_1[0] > 0 && poi_cube_1[0] < 1920 && poi_cube_1[1] > 0 && poi_cube_1[1] < 1080 &&
            poi_cube_2[0] > 0 && poi_cube_2[0] < 1920 && poi_cube_2[1] > 0 && poi_cube_2[1] < 1080)
            {
                poi_x = ((float)poi_cube_1[0] / 1920.0f + (float)poi_cube_2[0] / 1920.0f) / 2.0f;
                poi_y = 1.0f - ((float)poi_cube_1[1] / 1080.0f + (float)poi_cube_2[1] / 1080.0f) / 2.0f;
            }
            else if (poi_cube_1[0] > 0 && poi_cube_1[0] < 1920 && poi_cube_1[1] > 0 && poi_cube_1[1] < 1080 &&
                poi_cube_3[0] > 0 && poi_cube_3[0] < 1920 && poi_cube_3[1] > 0 && poi_cube_3[1] < 1080)
            {
                poi_x = ((float)poi_cube_1[0] / 1920.0f + (float)poi_cube_3[0] / 1920.0f) / 2.0f;
                poi_y = 1.0f - ((float)poi_cube_1[1] / 1080.0f + (float)poi_cube_3[1] / 1080.0f) / 2.0f;
            }
            else if (poi_cube_1[0] > 0 && poi_cube_1[0] < 1920 && poi_cube_1[1] > 0 && poi_cube_1[1] < 1080 &&
                poi_cube_4[0] > 0 && poi_cube_4[0] < 1920 && poi_cube_4[1] > 0 && poi_cube_4[1] < 1080)
            {
                poi_x = ((float)poi_cube_1[0] / 1920.0f + (float)poi_cube_4[0] / 1920.0f) / 2.0f;
                poi_y = 1.0f - ((float)poi_cube_1[1] / 1080.0f + (float)poi_cube_4[1] / 1080.0f) / 2.0f;
            }
        }
        else
        {
            if (poi_cube_1[0] > 0 && poi_cube_1[0] < 1920 && poi_cube_1[1] > 0 && poi_cube_1[1] < 1080)
            {
                poi_x = (float)poi_cube_1[0] / 1920.0f;
                poi_y = 1.0f - (float)poi_cube_1[1] / 1080.0f;
            }
        }

        if (Mathf.Abs(poi_x - poi_x_inc) > 0.01f)
        {
            poi_x_inc = poi_x_inc + (poi_x - poi_x_inc) / 4;
        }
        if (Mathf.Abs(poi_y - poi_y_inc) > 0.01f)
        {
            poi_y_inc = poi_y_inc + (poi_y - poi_y_inc) / 4;
        }
        change_pivot(poi_x_inc, poi_y_inc);
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    }

    void update_canvas()
    {
        centerforplane.z = plano.transform.position.z - z;
        if (keyboard_code == 72)//Back
        {
            plano.transform.Translate(Vector3.back * Time.deltaTime / 3, Space.World);
        }
        else if (keyboard_code == 89)//Forward
        {
            plano.transform.Translate(Vector3.forward * Time.deltaTime / 3, Space.World);
        }
        else if (keyboard_code == 71)//Left
        {
            plano.transform.RotateAround(centerforplane, Vector3.down, Time.deltaTime * 10);
        }
        else if (keyboard_code == 74)//Right
        {
            plano.transform.RotateAround(centerforplane, Vector3.up, Time.deltaTime * 10);
        }
        else if (keyboard_code == 84)//Up
        {
            plano.transform.Translate(Vector3.up * Time.deltaTime / 3, Space.World);
        }
        else if (keyboard_code == 85)//Down
        {
            plano.transform.Translate(Vector3.down * Time.deltaTime / 3, Space.World);
        }
    }

    void update_canvas_base_lock()
    {
        position = new double[3];
        position = mainscript.GetComponent<tcpconnect>().position;
        Vector3 aux = plano.transform.localPosition;
        aux.x = (float)position[0];// 1000.0f
        aux.y = (float)position[1];
        aux.z = (float)position[2];
        /*if (aux.x > -2.0f && aux.x < 2.0f &&
            aux.y > -2.0f && aux.y < 2.0f &&
            aux.z > 0.1f && aux.z < 2.0f)*/
        {
            plano.transform.localPosition = aux;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        width = mainscript.GetComponent<tcpconnect>().width;
        height = mainscript.GetComponent<tcpconnect>().height;
        //Define texture to be used
        tex = new Texture2D(width, height, TextureFormat.RGB24, false); //TextureFormat.RGBA32
        this.GetComponent<RawImage>().texture = tex;
        //----------------------------------------------------------------------

        //Second Screen init
        dual_screen = false;
        screen_2.SetActive(false);
        poi_x = 0.5f;
        poi_y = 0.5f;
        poi_x_inc = 0.5f;
        poi_y_inc = 0.5f;
        base_lock = false;
        two_marker_mode = false;
        single_key = false;
        crop_marker_model = false;
        x = plano.transform.position.x;
        y = plano.transform.position.y;
        z = plano.transform.position.z;
        centerforplane = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        if (mainscript.GetComponent<tcpconnect>().data_ready)
        {
            bytes_video = mainscript.GetComponent<tcpconnect>().video_bytes;
            tex.LoadRawTextureData(bytes_video);//Load image
            tex.Apply();//Apply texture to Canvas

            keyboard_code = mainscript.GetComponent<tcpconnect>().keyboard_key;
            poi_cube_1 = mainscript.GetComponent<tcpconnect>().poi_cube_1;
            poi_cube_2 = mainscript.GetComponent<tcpconnect>().poi_cube_2;
            poi_cube_3 = mainscript.GetComponent<tcpconnect>().poi_cube_3;
            poi_cube_4 = mainscript.GetComponent<tcpconnect>().poi_cube_4;

            update_crop();
            if (base_lock == true)
            {
                update_canvas_base_lock();
            }
            else
            {
                update_canvas();
            }
        }
    }
}
