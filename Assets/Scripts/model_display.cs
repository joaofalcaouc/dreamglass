﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class model_display : MonoBehaviour
{
    public GameObject mainscript;

    //Three Models
    public GameObject model_1;
    public GameObject model_2;
    public GameObject model_3;

    public GameObject model_info_1;
    public GameObject model_info_2;
    public GameObject[] Cubes;
    double[] position;
    double[] rotation;
    double[] s_position;
    double[] m_position;
    double[] m_rotation;
    public GameObject[] spheres;
    private Byte keyboard_code;
    private double[] guidedata;
    private bool single_key;

    void display_model(GameObject model, double[] position_model, double[] rotation_model)
    {

        Vector3 aux = model.transform.localPosition;
        aux.x = (float)position_model[0];// + 0.157f;
        aux.y = (float)position_model[1];// - 0.084f;
        aux.z = (float)position_model[2];// - 0.169f;

        Matrix4x4 aux_rot;
        aux_rot.m00 = (float)rotation_model[0]; aux_rot.m01 = (float)rotation_model[1]; aux_rot.m02 = (float)rotation_model[2]; aux_rot.m03 = (float)rotation_model[3];
        aux_rot.m10 = (float)rotation_model[4]; aux_rot.m11 = (float)rotation_model[5]; aux_rot.m12 = (float)rotation_model[6]; aux_rot.m13 = (float)rotation_model[7];
        aux_rot.m20 = (float)rotation_model[8]; aux_rot.m21 = (float)rotation_model[9]; aux_rot.m22 = (float)rotation_model[10]; aux_rot.m23 = (float)rotation_model[11];
        aux_rot.m30 = (float)rotation_model[12]; aux_rot.m31 = (float)rotation_model[13]; aux_rot.m32 = (float)rotation_model[14]; aux_rot.m33 = (float)rotation_model[15];
        aux_rot = aux_rot.transpose;

        var rotationMatrix = new Matrix4x4();
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                rotationMatrix[i, j] = aux_rot[i, j];
            }
        }
        rotationMatrix[3, 3] = 1f;

        var localToWorldMatrix = Matrix4x4.Translate(aux) * rotationMatrix;

        Vector3 scale;
        scale.x = new Vector4(localToWorldMatrix.m00, localToWorldMatrix.m10, aux_rot.m20, localToWorldMatrix.m30).magnitude;
        scale.y = new Vector4(localToWorldMatrix.m01, localToWorldMatrix.m11, aux_rot.m21, localToWorldMatrix.m31).magnitude;
        scale.z = new Vector4(localToWorldMatrix.m02, localToWorldMatrix.m12, aux_rot.m22, localToWorldMatrix.m32).magnitude;
        scale.x = -scale.x;
        model.transform.localScale = scale;

        Vector3 positioni;
        positioni.x = localToWorldMatrix.m03;
        positioni.y = localToWorldMatrix.m13;
        positioni.z = localToWorldMatrix.m23;
        model.transform.position = positioni;

        Vector3 forward;
        forward.x = localToWorldMatrix.m02;
        forward.y = localToWorldMatrix.m12;
        forward.z = localToWorldMatrix.m22;

        Vector3 upwards;
        upwards.x = localToWorldMatrix.m01;
        upwards.y = localToWorldMatrix.m11;
        upwards.z = localToWorldMatrix.m21;

        model.transform.rotation = Quaternion.LookRotation(forward, upwards);

    }

    void change_spheres() //Not used
    {
        for (int i = 0; i < 8 * 4; i = i + 4)
        {
            Vector3 aux_v;
            aux_v = spheres[i / 4].transform.localPosition;
            aux_v.x = (float)s_position[i];
            aux_v.y = (float)s_position[i + 1];
            aux_v.z = (float)s_position[i + 2];
            spheres[i / 4].transform.localPosition = aux_v;
        }
    }

    //COntrol guidance
    void guide_control()
    {
        //Keyboard control
        if(keyboard_code == 71 && single_key == false)
        {
            model_1.SetActive(true);
            model_2.SetActive(false);
            model_3.SetActive(false);
            single_key = true;
        }
        else if(keyboard_code == 72 && single_key == false)
        {
            model_1.SetActive(false);
            model_2.SetActive(true);
            model_3.SetActive(false);
            single_key = true;
        }
        else if (keyboard_code == 74 && single_key == false)
        {
            model_1.SetActive(false);
            model_2.SetActive(false);
            model_3.SetActive(true);
            single_key = true;
        }
        else if (keyboard_code == 48)
        {
            single_key = false;
        }

        //Color of model in alignment
        if (guidedata[2] == 3.0)
        {
            model_3.GetComponentInChildren<Renderer>().material.color = Color.green;
        }
        else if(guidedata[2] == 2.0)
        {
            model_3.GetComponentInChildren<Renderer>().material.color = Color.yellow;
        }
        else if (guidedata[2] == 1.0)
        {
            model_3.GetComponentInChildren<Renderer>().material.color = Color.red;
        }
        else if (guidedata[2] == 0.0)
        {
            model_3.GetComponentInChildren<Renderer>().material.color = Color.blue;
        }

        //Information of measurements
        Vector3 auxt;
        Camera[] auxic = Camera.allCameras;
        auxt = auxic[2].transform.position + auxic[2].transform.forward * 0.5f + auxic[2].transform.right * -0.2f;
        model_info_1.transform.localPosition = auxt;
        auxt = auxt + auxic[2].transform.up * 0.05f;
        model_info_2.transform.localPosition = auxt;

        if (guidedata[0] > 0)
        {
            string aux_guide_1 = guidedata[0].ToString();
            string aux_guide_2 = guidedata[1].ToString();

            model_info_1.GetComponent<Text>().text = aux_guide_1.Remove(4) + " mm";
            model_info_2.GetComponent<Text>().text = aux_guide_2.Remove(4) + " º";
        }
        else
        {
            model_info_1.GetComponent<Text>().text = guidedata[0].ToString() + " mm";
            model_info_2.GetComponent<Text>().text = guidedata[1].ToString() + " º";
        }

        //Display models according to the option (G,H,J)
        display_model(model_1,position,rotation);
        display_model(model_2, position, rotation);
        display_model(model_3, position, rotation);
    }

    void change_cubes()
    { 
        double[] auxiv = new double[3];
        auxiv = m_position;
        //auxiv[0] = auxiv[0] - 0.02f;
        display_model(Cubes[0], m_position, m_rotation);
    }

    // Start is called before the first frame update
    void Start()
    {
        /*model = new GameObject("Empty");
        model.AddComponent<MeshFilter>();
        model.AddComponent<MeshRenderer>();
        Mesh myMesh = (Mesh)Resources.Load("Models/scapula_centered_in_meters", typeof(Mesh));
        model.GetComponent<MeshFilter>().mesh = myMesh;*/

        model_1.GetComponentInChildren<Renderer>().material.color = Color.cyan;

        single_key = false;
        model_1.SetActive(false);
        model_2.SetActive(false);
        model_3.SetActive(false);

       
    }

    // Update is called once per frame
    void Update()
    {
        if (mainscript.GetComponent<tcpconnect>().data_ready)
        {
            //For alignment of virtual cubes (metric)
            m_position = mainscript.GetComponent<tcpconnect>().marker_position;
            m_rotation = mainscript.GetComponent<tcpconnect>().marker_rotation;
            //change_cubes();


            //Display guidance information
            guidedata = mainscript.GetComponent<tcpconnect>().guide_data;
            keyboard_code = mainscript.GetComponent<tcpconnect>().keyboard_key;
            position = mainscript.GetComponent<tcpconnect>().position;
            rotation = mainscript.GetComponent<tcpconnect>().rotation;
            guide_control();

            //s_position = mainscript.GetComponent<tcpconnect>().sphere_position;
            //change_spheres();
        }
    }
}
