clc;clear;

format long;

addpath(genpath('MLPnP_matlab_toolbox-master'));

dsnum = 1;

Koriginal = [1706.5 0 1400;0 1706.5 750; 0 0 1];

poses = load('markerposes_jf.dat');%poses = poses(:,5:8);
poses = poses((dsnum*160+1):(dsnum*160+80),5:8);
Nframes = size(poses,1)/4;

toolt = [-2.1328883458854961e+01, -7.4644706634181879e-01, -1.4972453194821367e+02, 1]';

poses = permute(reshape(poses.',[4 4 Nframes]),[2 1 3]);

l = 45;
cubepoint = [-l/2;-l/2;0;1];
track_points = [];

for i = 1:Nframes
track_points = [track_points ; poses(:,:,i)*toolt];
end

s_points = [
     875,900,1;
     1075,900,2;
     1300,900,3;
     1535,900,4;
     1735,900,5;
     875,800,6;
     1075,800,7;
     1300,800,8;
     1535,800,9;
     1735,800,10;
     875,700,11;
     1075,700,12;
     1300,700,13;
     1535,700,14;
     1735,700,15;
     875,600,16;
     1075,600,17;
     1300,600,18;
     1535,600,19;
     1735,600,20;
     ];
 s_points = [s_points(:,1)+20,s_points(:,2)+20];
 
 screen_points = [];
 screen_points = [2800-[s_points(:,1)],1500-[s_points(:,2)]];
 %screen_points = [[s_points(:,1)+20],[s_points(:,2)+20]];
 
 
 world_points = zeros(15,4);

 for i = 1:Nframes
    for j = 1:4
       world_points(i,j) = track_points((i-1)*4+j);
    end
 end

 world_points_l = world_points(:,1:3)/1000;
 
 poses = load('markerposes_jf.dat');
poses = poses((dsnum*160+81):(dsnum*160+160),5:8);%poses = poses(:,5:8);
Nframes = size(poses,1)/4;

poses = permute(reshape(poses.',[4 4 Nframes]),[2 1 3]);

l = 45;
cubepoint = [-l/2;-l/2;0;1];
track_points = [];

for i = 1:Nframes
track_points = [track_points ; poses(:,:,i)*toolt];
end

world_points = zeros(15,4);

 for i = 1:Nframes
    for j = 1:4
       world_points(i,j) = track_points((i-1)*4+j);
    end
 end

 world_points_r = world_points(:,1:3)/1000;
 
 
 
world_points_l(:,1) = -world_points_l(:,1);
world_points_r(:,1) = -world_points_r(:,1);

[ki,ka,ri,ti] = getPerpProj(screen_points,world_points_l,0.1,1000,2800,1500);
[kj,kb,rj,tj] = getPerpProj(screen_points,world_points_r,0.1,1000,2800,1500);

world_points_r = world_points_r';
u = Koriginal\[screen_points.';ones(1,20)];
[R0 t0 error0 flag] = OPnP(world_points_r(1:3,:),u);
T = [R0 t0; 0 0 0 1];
rj = -R0;
tj = -t0;
kj = KtoKi(Koriginal);;
world_points_r = world_points_r';

world_points_l = world_points_l';
u = Koriginal\[screen_points.';ones(1,20)];
[R0 t0 error0 flag] = OPnP(world_points_l(1:3,:),u);
T = [R0 t0; 0 0 0 1];
ri = -R0;
ti = -t0;
ki = KtoKi(Koriginal);
world_points_l = world_points_l';

TI = [ri,ti;0 0 0 1];
TJ = [rj,tj;0 0 0 1];
dt = TI/TJ
error_rot = rotm2axang(dt(1:3,1:3));
error_rot(4)*180/pi

norm(dt(1:3,4))%erro em t


znear = 0.1;
zfar = 1000;

%left
transform = [ri,ti; 0 0 0 1];
ki(1,1) = -ki(1,1);
aux_calc = ki*transform*[world_points_l,ones(20,1)]';
temp2 = aux_calc./aux_calc(4,:);
tempf = [
(2800/2)*temp2(1,:) + 0.0 + (2800/2);
(1500/2)*temp2(2,:) + 0.0 + (1500/2);
((zfar - znear)/2)*temp2(3,:)+((zfar + znear)/2)
];
error_l = abs(s_points(:,1:2)'-tempf(1:2,:));
er_vec_l = sqrt(error_l(1,:).^2 + error_l(2,:).^2)
mean(er_vec_l)
std(er_vec_l)

%right
transform = [rj,tj; 0 0 0 1];
kj(1,1) = -kj(1,1);
aux_calc = kj*transform*[world_points_r,ones(20,1)]';
temp2 = aux_calc./aux_calc(4,:);
tempf = [
(2800/2)*temp2(1,:) + 0.0 + (2800/2);
(1500/2)*temp2(2,:) + 0.0 + (1500/2);
((zfar - znear)/2)*temp2(3,:)+((zfar + znear)/2)
];
error_r = abs(s_points(:,1:2)'-tempf(1:2,:));
er_vec_r = sqrt(error_r(1,:).^2 + error_r(2,:).^2)
mean(er_vec_r)
std(er_vec_r)
% figure;
% plot(1:15,error_l(1,:),'*')
% hold on
% plot(1:15,error_l(2,:),'*')
% hold off
% 
% figure;
% plot(1:15,error_r(1,:),'*')
% hold on
% plot(1:15,error_r(2,:),'*')
% hold off

% figure;
% boxplot(error_l')
% xlabel('axis');
% ylabel('pixels');
% figure;
% boxplot(error_r')
% xlabel('axis');
% ylabel('pixels');

s = struct('Ext_l',zeros(4,4),'Ext_r',zeros(4,4),'Int_l',zeros(4,4),'Int_r',zeros(4,4));
s.Ext_l = [ri,ti;0 0 0 1];
s.Ext_r = [rj,tj;0 0 0 1];
s.Int_l = ki;
s.Int_r = kj;

savejson('',s,'glasses_int_ext.json');
