function [Ki] = KtoKi(K)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

n = 0.1;
f = 1000;
width = 2800;
height = 1500;

x0 = 0;%camera image origin
y0 = 0;
y_up_down = 1;%1 if y up and -1 if y down

Ki = [  
    2*K(1,1)/width, 0, 1.0+(-2.0*K(1,3) + 2*x0)/width, 0;
    0, y_up_down*-2*K(2,2)/height, (y_up_down*-2.0*K(2,3) + 2*y0)/height + y_up_down*1.0, 0;
    0, 0, (f+n)/(n-f), (2*f*n)/(n-f);
    0, 0, -1, 0
];

%This is to obtain the unity like matrix, with positive values
Ki(1,:) = -Ki(1,:);
Ki(2,:) = -Ki(2,:);
end

