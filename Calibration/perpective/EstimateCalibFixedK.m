clear,clc,close all
addpath(genpath('MLPnP_matlab_toolbox-master'));

results;

s_points = [
     875,900,1;
     1075,900,2;
     1300,900,3;
     1535,900,4;
     1735,900,5;
     875,800,6;
     1075,800,7;
     1300,800,8;
     1535,800,9;
     1735,800,10;
     875,700,11;
     1075,700,12;
     1300,700,13;
     1535,700,14;
     1735,700,15;
     875,600,16;
     1075,600,17;
     1300,600,18;
     1535,600,19;
     1735,600,20;
     ];
s_points = [s_points(:,1)+20,s_points(:,2)+20];
 
screen_points = [];
screen_points = [2800-[s_points(:,1)],1500-[s_points(:,2)]];
Koriginal = [1706.5 0 1400;0 1706.5 750; 0 0 1];

count = 0;
for testid = 4:6;
    count = count+1;
    
    ka = eval(['ka' num2str(testid)])*1000;
    ri =eval(['ri' num2str(testid)]);
    [a b c] = svd(ri);
    ri  = a*c.';
    ti =eval(['ti' num2str(testid)]);
    Kleft_all(:,:,count) = ka;
    
    world_points = getWorldPointsFromMarkerPoses(['markerposes_n' num2str(testid) 'l.dat']);
    
    world_points_l = world_points;
    
    T = [ri ti;0 0 0 1];
    
    out = computeAvgReprojError(world_points, screen_points, T, ka)
    out = computeAvgReprojError(world_points, screen_points, T, Koriginal)
    
    Tcalib_l(:,:,count) = T;
    aux = rotm2axang(T(1:3,1:3));
    rot_calib_l(:,count) = aux(1:3)*aux(4)*180/pi;
    fx_all_l(count) = ka(1,1); fy_all_l(count) = ka(2,2);
    cx_all_l(count) =  ka(1,3); cy_all_l(count) = ka(2,3);
    
    %estimate R,t by fixing K
    % intrinsics = cameraIntrinsics([Koriginal(1,1) Koriginal(2,2)],Koriginal(1:2,3).', [1080 1920]);
    % intrinsics = cameraIntrinsics([ka(1,1) ka(2,2)],ka(1:2,3).', [1080 1920]);
    % [worldOrientation,worldLocation, inliers] = estimateWorldCameraPose(screen_points,world_points(1:3,:).',intrinsics, 'MaxReprojectionError',1e10);
    %
    % T =  [worldOrientation.' worldLocation.';0 0 0 1];
    % out = computeAvgReprojError(world_points, screen_points, T, ka)
    
    u = ka\[screen_points.';ones(1,15)];
    [R0 t0 error0 flag] = OPnP(world_points(1:3,:),u);
    T = [R0 t0; 0 0 0 1];
    out = computeAvgReprojError(world_points, screen_points, T, ka)
    
    u = Koriginal\[screen_points.';ones(1,15)];
    [R0 t0 error0 flag] = OPnP(world_points(1:3,:),u);
    T = [R0 t0; 0 0 0 1];
    out = computeAvgReprojError(world_points, screen_points, T, Koriginal)
    
    Test_l(:,:,count) = T;
    aux = rotm2axang(T(1:3,1:3));
    rot_est_l(:,count) = aux(1:3)*aux(4)*180/pi;
    
    
    
    %% right
    
    ka = eval(['kb' num2str(testid)])*1000;
    ri =eval(['rj' num2str(testid)]);
    [a b c] = svd(ri);
    ri  = a*c.';
    ti =eval(['tj' num2str(testid)]);
    Kright_all(:,:,count) = ka;
    
    world_points = getWorldPointsFromMarkerPoses(['markerposes_n' num2str(testid) 'r.dat']);
    
    world_points_r = world_points;
    
    T = [ri ti;0 0 0 1];
    
    out = computeAvgReprojError(world_points, screen_points, T, ka)
    out = computeAvgReprojError(world_points, screen_points, T, Koriginal)
    
    Tcalib_r(:,:,count) = T;
    aux = rotm2axang(T(1:3,1:3));
    rot_calib_r(:,count) = aux(1:3)*aux(4)*180/pi;
    fx_all_r(count) = ka(1,1); fy_all_r(count) = ka(2,2);
    cx_all_r(count) =  ka(1,3); cy_all_r(count) = ka(2,3);
    
    %estimate R,t by fixing K
    % intrinsics = cameraIntrinsics([Koriginal(1,1) Koriginal(2,2)],Koriginal(1:2,3).', [1080 1920]);
    % intrinsics = cameraIntrinsics([ka(1,1) ka(2,2)],ka(1:2,3).', [1080 1920]);
    % [worldOrientation,worldLocation, inliers] = estimateWorldCameraPose(screen_points,world_points(1:3,:).',intrinsics, 'MaxReprojectionError',1e10);
    %
    % T =  [worldOrientation.' worldLocation.';0 0 0 1];
    % out = computeAvgReprojError(world_points, screen_points, T, ka)
    
    u = ka\[screen_points.';ones(1,15)];
    [R0 t0 error0 flag] = OPnP(world_points(1:3,:),u);
    T = [R0 t0; 0 0 0 1];
    out = computeAvgReprojError(world_points, screen_points, T, ka)
    
    u = Koriginal\[screen_points.';ones(1,15)];
    [R0 t0 error0 flag] = OPnP(world_points(1:3,:),u);
    T = [R0 t0; 0 0 0 1];
    out = computeAvgReprojError(world_points, screen_points, T, Koriginal)
    
    Test_r(:,:,count) = T;
    aux = rotm2axang(T(1:3,1:3));
    rot_est_r(:,count) = aux(1:3)*aux(4)*180/pi;
    
    
    %% force R,t, K
    
    Tin = Tcalib_l(:,:,count);
    aux = rotm2axang(Tin(1:3,1:3));
    aux = aux(1:3)*aux(4);
    
    x0 = [0.062; aux.'; Tin(1:3,4)];
    options = optimoptions(@lsqnonlin,'Algorithm','levenberg-marquardt');
   [ x,RESNORM,RESIDUAL] = lsqnonlin(@(x) ComputeErrorFixRtK(x, Koriginal, world_points_l, world_points_r, screen_points) ,x0,[],[],options);
    
   %final solution:
   Tdelta = eye(4);
   Tdelta(1,4) = x(1);
   
   Tleft = eye(4);
   aux = x(2:4);
   aux = [aux/norm(aux); norm(aux)].';
   Tleft(1:3,1:3) = axang2rotm(aux);
   Tleft(1:3,4) = x(5:7);
   
   Tright = Tleft * Tdelta;
   
end


figure, hold on; scatter3(rot_est_l(1,:), rot_est_l(2,:), rot_est_l(3,:),'filled');
scatter3(rot_calib_l(1,:), rot_calib_l(2,:), rot_calib_l(3,:),'filled');
axis equal

figure, hold on; scatter3(Test_l(1,4,:), Test_l(2,4,:), Test_l(3,4,:),'filled');
scatter3(Tcalib_l(1,4,:), Tcalib_l(2,4,:), Tcalib_l(3,4,:),'filled');
axis equal

figure, hold on; scatter3(rot_est_r(1,:), rot_est_r(2,:), rot_est_r(3,:),'filled');
scatter3(rot_calib_r(1,:), rot_calib_r(2,:), rot_calib_r(3,:),'filled');
axis equal

figure, hold on; scatter3(Test_r(1,4,:), Test_r(2,4,:), Test_r(3,4,:),'filled');
scatter3(Tcalib_r(1,4,:), Tcalib_r(2,4,:), Tcalib_r(3,4,:),'filled');
axis equal

%figure, boxplot([fx_all; fy_all; cx_all;cy_all].')
figure, hold on; scatter([1 1 1], [fx_all_l],'*')
scatter(2*[1 1 1], [fy_all_l],'*')
scatter(3*[1 1 1], [cx_all_l],'*')
scatter(4*[1 1 1], [cy_all_l],'*')

figure, hold on; scatter([1 1 1], [fx_all_r],'*')
scatter(2*[1 1 1], [fy_all_r],'*')
scatter(3*[1 1 1], [cx_all_r],'*')
scatter(4*[1 1 1], [cy_all_r],'*')


for i=1:3
    
    dTcalib(:,:,i)  = Tcalib_l(:,:,i)/Tcalib_r(:,:,i);
    dTest(:,:,i)  = Test_l(:,:,i)/Test_r(:,:,i);
    
end

Kleft_avg = mean(Kleft_all,3);
Tcalib_l_avg = mean(Tcalib_l,3);
aux = rot_calib_l*pi/180;
aux = mean(aux,2);
aux = [aux/norm(aux); norm(aux)].';
Tcalib_l_avg(1:3,1:3) = axang2rotm(aux);

Kright_avg = mean(Kright_all,3);
Tcalib_r_avg = mean(Tcalib_r,3);
aux = rot_calib_r*pi/180;
aux = mean(aux,2);
aux = [aux/norm(aux); norm(aux)].';
Tcalib_r_avg(1:3,1:3) = axang2rotm(aux);

dTcalib_avg = Tcalib_l_avg/Tcalib_r_avg;



Test_l_avg = mean(Test_l,3);
aux = rot_est_l*pi/180;
aux = mean(aux,2);
aux = [aux/norm(aux); norm(aux)].';
Test_l_avg(1:3,1:3) = axang2rotm(aux);

Test_r_avg = mean(Test_r,3);
aux = rot_est_r*pi/180;
aux = mean(aux,2);
aux = [aux/norm(aux); norm(aux)].';
Test_r_avg(1:3,1:3) = axang2rotm(aux);

dTest_avg = Test_l_avg/Test_r_avg;

function errs = ComputeErrorFixRtK(x, K, world_points_l, world_points_r, screen_points)

Tgt = eye(4);
Tgt(1,4) = x(1);


Tleft = eye(4);
aux = x(2:4);
aux = [aux/norm(aux); norm(aux)].';
Tleft(1:3,1:3) = axang2rotm(aux);
Tleft(1:3,4) = x(5:7);

Tright = Tleft * Tgt;

difs_l = computeReprojError(world_points_l, screen_points, Tleft, K);
difs_r = computeReprojError(world_points_r, screen_points, Tright, K);

errs = [difs_l(:);difs_r(:)];
end

function world_points = getWorldPointsFromMarkerPoses(markerposes_file)

poses = load(markerposes_file);
poses = poses(:,5:8);
Nframes = size(poses,1)/4;

toolt = [-2.1328883458854961e+01, -7.4644706634181879e-01, -1.4972453194821367e+02, 1]';
poses = permute(reshape(poses.',[4 4 Nframes]),[2 1 3]);

track_points = [];

for i = 1:Nframes
    track_points = [track_points ; poses(:,:,i)*toolt];
end


world_points = reshape(track_points, [4 15]);
world_points(1:3,:)= world_points(1:3,:)/1000;
world_points(1,:) = -world_points(1,:);

end


function difs = computeReprojError(world_points, screen_points, T, K)
wp = T*world_points;
pts_proj = K*wp(1:3,:);
pts_proj = pts_proj(1:2,:)./pts_proj(3,:);

difs = pts_proj-screen_points.';

end

function out = computeAvgReprojError(world_points, screen_points, T, K)
difs = computeReprojError(world_points, screen_points, T, K);
errors = vecnorm(difs,2,1);
out = mean(errors);

end