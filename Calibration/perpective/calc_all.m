mean_l=[];
std_l=[];

mean_r=[];
std_r=[];

error_vec_l = [];
error_vec_r = [];

save_ri = [];
save_rj = [];
save_ti = [];
save_tj = [];
save_ki = [];
save_kj = [];

error_rot=[0,0,0,0,0,0];
error_trans=[0,0,0,0,0,0];
ipd=[];

subj = 'jf';
for id = 0:1:5
    
[ki,ka,ri,ti,kj,kb,rj,tj,dt,er_vec_l,er_vec_r] = calc_calib_fixed(id,['markerposes_' subj '.dat']);

save_ri(:,:,id+1) = ri;
save_rj(:,:,id+1) = rj;
save_ti(:,id+1) = ti;
save_tj(:,id+1) = tj;
save_ki(:,:,id+1) = ki;
save_kj(:,:,id+1) = kj;

error_rotation = rotm2axang(dt(1:3,1:3));
error_rot(id+1) = error_rotation(4)*180/pi;

error_trans(id+1) = norm(dt(1:3,4));%erro em t

ipd(id+1) = dt(1,4);

error_vec_l(id+1,:) = er_vec_l;
error_vec_r(id+1,:) = er_vec_r;

mean_l(id+1) = mean(er_vec_l);
std_l(id+1) = std(er_vec_l);

mean_r(id+1) = mean(er_vec_r);
std_r(id+1) = std(er_vec_r);
end

%save(['spaam_' subj '.mat'], 'error_vec_l','error_vec_r','save_ri','save_rj','save_ti','save_tj','save_ki','save_kj');
%save(['spaam_avg_' subj '.mat'], 'error_vec_l','error_vec_r','save_ri','save_rj','save_ti','save_tj','save_ki','save_kj');
%save(['spaam_forced_' subj '.mat'], 'error_vec_l','error_vec_r','save_ri','save_rj','save_ti','save_tj','save_ki','save_kj');
save(['spaam_fixed_' subj '.mat'], 'error_vec_l','error_vec_r','save_ri','save_rj','save_ti','save_tj','save_ki','save_kj');

format short
error_table = [mean_l',std_l',mean_r',std_r']

error_rottrans_ipd = [error_rot',error_trans',ipd']

% figure;
% boxplot(error_vec_l')
% xlabel('Dataset');
% ylabel('Pixels');
% figure;
% boxplot(error_vec_r')
% xlabel('Dataset');
% ylabel('Pixels');