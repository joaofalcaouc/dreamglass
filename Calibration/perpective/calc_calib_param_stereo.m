clc;clear;
addpath(genpath('MLPnP_matlab_toolbox-master'));

poses = load('markerposes_stereo.dat');
poses = poses(:,5:8);
Nframes = size(poses,1)/4;

toolt = [-2.1328883458854961e+01, -7.4644706634181879e-01, -1.4972453194821367e+02, 1]';

poses = permute(reshape(poses.',[4 4 Nframes]),[2 1 3]);

l = 45;
cubepoint = [-l/2;-l/2;0;1];
track_points = [];

for i = 1:Nframes
track_points = [track_points ; poses(:,:,i)*toolt];
end

r_s= [
-0.5899996  0.185
-0.3799998  0.185
-0.17       0.175
0.02999996  0.175
0.1799999   0.175
-0.5849996  0.009999992
-0.3799998  0.009999992
-0.205      0.009999992
0.02499996  0.009999992
0.2049999   0.009999992
-0.5899996  -0.165
-0.3949998  -0.165
-0.18       -0.165
0.2299999   -0.155
0.2149999   -0.17];

l_s = [
    -0.288 0.17
    -0.088 0.17
    0.112 0.17
    0.312 0.17
    0.512 0.17
    -0.288 0.0
    -0.088 0.0
    0.112 0.0
    0.312 0.0
    0.512 0.0
    -0.288 -0.17
    -0.088 -0.17
    0.112 -0.17
    0.312 -0.17
    0.512 -0.17
];

r_s = r_s.*1000;
l_s = l_s.*1000;

r_s(:,1) = r_s(:,1)+1400;
r_s(:,2) = r_s(:,2)+750;
l_s(:,1) = l_s(:,1)+1400;
l_s(:,2) = l_s(:,2)+750;

screen_points_l = [];
screen_points_r = [];
screen_points_l = [2800-[l_s(:,1)],1500-[l_s(:,2)]];
screen_points_r = [2800-[r_s(:,1)],1500-[r_s(:,2)]];

 world_points = zeros(15,4);

 for i = 1:Nframes
    for j = 1:4
       world_points(i,j) = track_points((i-1)*4+j);
    end
 end

 world_points_l = world_points(:,1:3)/1000;
 
 poses = load('markerposes_stereo.dat');
poses = poses(:,5:8);
Nframes = size(poses,1)/4;

poses = permute(reshape(poses.',[4 4 Nframes]),[2 1 3]);

l = 45;
cubepoint = [-l/2;-l/2;0;1];
track_points = [];

for i = 1:Nframes
track_points = [track_points ; poses(:,:,i)*toolt];
end

world_points = zeros(15,4);

 for i = 1:Nframes
    for j = 1:4
       world_points(i,j) = track_points((i-1)*4+j);
    end
 end

 world_points_r = world_points(:,1:3)/1000;
 
 
 
world_points_l(:,1) = -world_points_l(:,1);
world_points_r(:,1) = -world_points_r(:,1);

[ki,ka,ri,ti] = getPerpProj(screen_points_l,world_points_l,0.1,1000,2800,1500);
[kj,kb,rj,tj] = getPerpProj(screen_points_r,world_points_r,0.1,1000,2800,1500);

ti(1) = -ti(1);
% world_points_r = world_points_r';
% u = ka\[screen_points_r.';ones(1,15)];
% [R0 t0 error0 flag] = OPnP(world_points_r(1:3,:),u);
% T = [R0 t0; 0 0 0 1];
% rj = R0;
% tj = -t0;
% kj = ki;
% world_points_r = world_points_r';

% world_points_l = world_points_l';
% u = kb\[screen_points_l.';ones(1,15)];
% [R0 t0 error0 flag] = OPnP(world_points_l(1:3,:),u);
% T = [R0 t0; 0 0 0 1];
% ri = R0;
% ti = -t0;
% ki = ki;
% world_points_l = world_points_l';


% aux = rotm2axang(T(1:3,1:3));
% rot_est_r(:,1) = aux(1:3)*aux(4)*180/pi;
% aux = rot_est_r*pi/180;
% aux = mean(aux,2);
% aux = [aux/norm(aux); norm(aux)].';
% Test_r_avg(1:3,1:3) = axang2rotm(aux);
%out = computeAvgReprojError(world_points, screen_points, T, ka)
% intrinsics = cameraIntrinsics([ka(1,1) ka(2,2)],ka(1:2,3).', [1500 2800]);%[1080 1920]
% [worldOrientation,worldLocation, inliers] = estimateWorldCameraPose(screen_points_r,world_points_r,intrinsics)%, 'MaxReprojectionError',1e10);
% T =  [worldOrientation.' worldLocation.';0 0 0 1];
%out = computeAvgReprojError(world_points_r, screen_points_r, T, ka)


TI = [ri,ti;0 0 0 1];
TJ = [rj,tj;0 0 0 1];
dt = TI/TJ
error_rot = rotm2axang(dt(1:3,1:3));
error_rot(4)*180/pi
norm(dt(1:3,4))%erro em t



znear = 0.1;
zfar = 1000;

%left
transform = [ri,ti; 0 0 0 1];
ki(1,1) = -ki(1,1);
aux_calc = ki*transform*[world_points_l,ones(15,1)]';
temp2 = aux_calc./aux_calc(4,:);
tempf = [
(2800/2)*temp2(1,:) + 0.0 + (2800/2);
(1500/2)*temp2(2,:) + 0.0 + (1500/2);
((zfar - znear)/2)*temp2(3,:)+((zfar + znear)/2)
];
error_l = abs(l_s(:,1:2)'-tempf(1:2,:))

%right
transform = [rj,tj; 0 0 0 1];
kj(1,1) = -kj(1,1);
aux_calc = kj*transform*[world_points_r,ones(15,1)]';
temp2 = aux_calc./aux_calc(4,:);
tempf = [
(2800/2)*temp2(1,:) + 0.0 + (2800/2);
(1500/2)*temp2(2,:) + 0.0 + (1500/2);
((zfar - znear)/2)*temp2(3,:)+((zfar + znear)/2)
];
error_r = abs(r_s(:,1:2)'-tempf(1:2,:))

% figure;
% plot(1:15,error_l(1,:),'*')
% hold on
% plot(1:15,error_l(2,:),'*')
% hold off
% 
% figure;
% plot(1:15,error_r(1,:),'*')
% hold on
% plot(1:15,error_r(2,:),'*')
% hold off

% figure;
% boxplot(error_l')
% xlabel('axis');
% ylabel('pixels');
% figure;
% boxplot(error_r')
% xlabel('axis');
% ylabel('pixels');

s = struct('Ext_l',zeros(4,4),'Ext_r',zeros(4,4),'Int_l',zeros(4,4),'Int_r',zeros(4,4))
s.Ext_l = [ri,ti;0 0 0 1];
s.Ext_r = [rj,tj;0 0 0 1];
s.Int_l = ki;
s.Int_r = kj;

savejson('',s,'glasses_int_ext.json');