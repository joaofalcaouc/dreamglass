clear;clc;
format short;
subj = 'jf';
poses = load(['markerposes_error_' subj '_forced.dat']);
error_translation = [];
error_rotation = [];
for j = 0:1:5
dsnum = j;
poses_bm = poses((dsnum*72+1):(dsnum*72+72),1:4);
poses_tm = poses((dsnum*72+1):(dsnum*72+72),5:8);
Nframes = size(poses_bm,1)/4;

poses_bm = permute(reshape(poses_bm.',[4 4 Nframes]),[2 1 3]);
poses_tm = permute(reshape(poses_tm.',[4 4 Nframes]),[2 1 3]);

% track_points_bm = [];
% track_points_tm = [];
% 
% for i = 1:Nframes
% track_points_bm = [track_points_bm ; (poses_bm(:,:,i)*toolt)'];
% end
% for i = 1:Nframes
% track_points_tm = [track_points_tm ; (poses_tm(:,:,i)*toolt)'];
% end

rot_dif = [];
trans_dif = [];

d1 = 0.08*1000;
d2 = 0.16*1000;
transls_gt = [-d1 d1 0;-d1 0 0; -d1 0 d1; d1 d1 0; d1 0 0; d1 0 d1; -d2 d2 0;-d2 0 0; -d2 0 d2; d2 d2 0; d2 0 0; d2 0 d2; -d2 d2 0;-d2 0 0; -d2 0 d2; d2 d2 0; d2 0 0; d2 0 d2;];
for i = 1:Nframes
    dt = poses_bm(:,:,i)\poses_tm(:,:,i);
    t_gt = eye(4);
    t_gt(1:3,4) = transls_gt(i,:).';
    dt_ = dt\t_gt;
    dt_save(:,i) = abs(dt_(1:3,4));
    error_rot = rotm2axang(dt_(1:3,1:3));
    rot_dif(i) = error_rot(4)*180/pi;

    if (abs(rot_dif(i) - 180) < 30)
        rot_dif(i) = 180-rot_dif(i);
    end
    
     if (abs(rot_dif(i) - 90) < 30)
        rot_dif(i) = abs(90-rot_dif(i));
     end
%     if(i == 7 && j == 0)
%         rot_dif(i) = abs(180-rot_dif(i));
%     end
    trans_dif(i) = norm(dt_(1:3,4))/norm(transls_gt(i,:))*100;
end

error_translation = [ error_translation;trans_dif];
error_rotation = [ error_rotation;rot_dif];
end

%save_r_avg = [error_rotation(1,:);error_rotation(3,:);error_rotation(5,:)];
%save_t_avg = [error_translation(1,:);error_translation(3,:);error_translation(5,:)];

%save_r_fixed = error_rotation;
%save_t_fixed = error_translation;

save(['errorcube_avg_' subj '.mat'], 'save_r_avg','save_t_avg');

% colors = {'r','g','b','c'};
% h = boxplot2(allerrs_l);
% for ii = 1:length(methods)
%     structfun(@(x) set(x(ii,:), 'color', colors{ii}, ...
%         'markeredgecolor', colors{ii}), h);
% end
% set([h.lwhis h.uwhis], 'linestyle', '-');
% set(h.out, 'marker', '.');
% xticks(1:size(allerrs_l,1))
% xlabel('Dataset');
% ylabel('Reprojection error (pix)');
% legend('SPAAM', 'SPAAM_{avg}', 'SPAAM_{fact}','SPAAM_{fixed}');
% 
% fig = gcf;
% fig.PaperUnits = 'inches';
% fig.PaperPosition = [0 0 10 5];
% print(['Figs/ReprojError' subjs{s} '_l'],'-dpng','-r0')
% 
% close

%  figure, plot(dt_save(1,:),'b');
%  hold on, plot(dt_save(2,:),'r');
%  hold on, plot(dt_save(3,:),'g');