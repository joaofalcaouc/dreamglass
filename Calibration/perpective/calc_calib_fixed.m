function [ki,ka,ri,ti,kj,kb,rj,tj,dt,er_vec_l,er_vec_r] = calc_calib_fixed(dsnum, dataset_name)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
format long;

poses = load(dataset_name);%poses = poses(:,5:8);
poses = poses((dsnum*160+1):(dsnum*160+80),5:8);
Nframes = size(poses,1)/4;

toolt = [-2.1328883458854961e+01, -7.4644706634181879e-01, -1.4972453194821367e+02, 1]';

poses = permute(reshape(poses.',[4 4 Nframes]),[2 1 3]);

l = 45;
cubepoint = [-l/2;-l/2;0;1];
track_points = [];

for i = 1:Nframes
track_points = [track_points ; poses(:,:,i)*toolt];
end

s_points = [
     875,900,1;
     1075,900,2;
     1300,900,3;
     1535,900,4;
     1735,900,5;
     875,800,6;
     1075,800,7;
     1300,800,8;
     1535,800,9;
     1735,800,10;
     875,700,11;
     1075,700,12;
     1300,700,13;
     1535,700,14;
     1735,700,15;
     875,600,16;
     1075,600,17;
     1300,600,18;
     1535,600,19;
     1735,600,20;
     ];
 s_points = [s_points(:,1)+20,s_points(:,2)+20];
 
 screen_points = [];
 screen_points = [2800-[s_points(:,1)],1500-[s_points(:,2)]];
 %screen_points = [[s_points(:,1)+20],[s_points(:,2)+20]];
 
 
 world_points = zeros(15,4);

 for i = 1:Nframes
    for j = 1:4
       world_points(i,j) = track_points((i-1)*4+j);
    end
 end

 world_points_l = world_points(:,1:3)/1000;
 
 poses = load(dataset_name);
poses = poses((dsnum*160+81):(dsnum*160+160),5:8);%poses = poses(:,5:8);
Nframes = size(poses,1)/4;

poses = permute(reshape(poses.',[4 4 Nframes]),[2 1 3]);

l = 45;
cubepoint = [-l/2;-l/2;0;1];
track_points = [];

for i = 1:Nframes
track_points = [track_points ; poses(:,:,i)*toolt];
end

world_points = zeros(15,4);

 for i = 1:Nframes
    for j = 1:4
       world_points(i,j) = track_points((i-1)*4+j);
    end
 end

 world_points_r = world_points(:,1:3)/1000;
 
 
 
world_points_l(:,1) = -world_points_l(:,1);
world_points_r(:,1) = -world_points_r(:,1);

[ki,ka,ri,ti] = getPerpProj(screen_points,world_points_l,0.1,1000,2800,1500);
[kj,kb,rj,tj] = getPerpProj(screen_points,world_points_r,0.1,1000,2800,1500);

%-------Force RT----------
Koriginal = [1706.5 0 1400;0 1706.5 750; 0 0 1];
Tin = [-ri -ti;0 0 0 1];
aux = rotm2axang(Tin(1:3,1:3));
aux = aux(1:3)*aux(4);

x0 = [0.062; aux.'; Tin(1:3,4)];
options = optimoptions(@lsqnonlin,'Algorithm','levenberg-marquardt');
[ x,RESNORM,RESIDUAL] = lsqnonlin(@(x) ComputeErrorFixRtK(x, Koriginal, world_points_l, world_points_r, screen_points) ,x0,[],[],options);

%final solution:
Tdelta = eye(4);
Tdelta(1,4) = x(1);

Tleft = eye(4);
aux = x(2:4);
aux = [aux/norm(aux); norm(aux)].';
Tleft(1:3,1:3) = axang2rotm(aux);
Tleft(1:3,4) = x(5:7);

Tright = Tleft * Tdelta;

dt = inv(Tdelta);

ri = -Tleft(1:3,1:3);
ti = -Tleft(1:3,4);
rj = -Tright(1:3,1:3);
tj = -Tright(1:3,4);
ka = Koriginal;
kb = Koriginal;
ki = KtoKi(Koriginal);
kj = KtoKi(Koriginal);
%-------------------------

TI = [ri,ti;0 0 0 1];
TJ = [rj,tj;0 0 0 1];
%dt = TI/TJ;
%error_rot = rotm2axang(dt(1:3,1:3));
%error_rot(4)*180/pi

%error_t = norm(dt(1:3,4))%erro em t


znear = 0.1;
zfar = 1000;

%left
transform = [ri,ti; 0 0 0 1];
ki(1,1) = -ki(1,1);
aux_calc = ki*transform*[world_points_l,ones(20,1)]';
temp2 = aux_calc./aux_calc(4,:);
tempf = [
(2800/2)*temp2(1,:) + 0.0 + (2800/2);
(1500/2)*temp2(2,:) + 0.0 + (1500/2);
((zfar - znear)/2)*temp2(3,:)+((zfar + znear)/2)
];
error_l = abs(s_points(:,1:2)'-tempf(1:2,:));
er_vec_l = sqrt(error_l(1,:).^2 + error_l(2,:).^2);
% mean(error_l(1,:))
% std(error_l(1,:))
% mean(error_l(2,:))
% std(error_l(2,:))

%right
transform = [rj,tj; 0 0 0 1];
kj(1,1) = -kj(1,1);
aux_calc = kj*transform*[world_points_r,ones(20,1)]';
temp2 = aux_calc./aux_calc(4,:);
tempf = [
(2800/2)*temp2(1,:) + 0.0 + (2800/2);
(1500/2)*temp2(2,:) + 0.0 + (1500/2);
((zfar - znear)/2)*temp2(3,:)+((zfar + znear)/2)
];
error_r = abs(s_points(:,1:2)'-tempf(1:2,:));
er_vec_r = sqrt(error_r(1,:).^2 + error_r(2,:).^2);
% mean(error_r(1,:))
% std(error_r(1,:))
% mean(error_r(2,:))
% std(error_r(2,:))

function errs = ComputeErrorFixRtK(x, K, world_points_l, world_points_r, screen_points)

Tgt = eye(4);
Tgt(1,4) = x(1);


Tleft = eye(4);
aux = x(2:4);
aux = [aux/norm(aux); norm(aux)].';
Tleft(1:3,1:3) = axang2rotm(aux);
Tleft(1:3,4) = x(5:7);

Tright = Tleft * Tgt;

difs_l = computeReprojError(world_points_l, screen_points, Tleft, K);
difs_r = computeReprojError(world_points_r, screen_points, Tright, K);

errs = [difs_l(:);difs_r(:)];
end


function difs = computeReprojError(world_points, screen_points, T, K)
wp = T*[world_points,ones(20,1)]';
pts_proj = K*wp(1:3,:);
pts_proj = pts_proj(1:2,:)./pts_proj(3,:);

difs = pts_proj-screen_points.';

end
end

