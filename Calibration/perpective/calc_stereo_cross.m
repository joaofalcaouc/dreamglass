clear;clc;
format shortG;

s_points = [
     875,900,1;
     1075,900,2;
     1300,900,3;
     1535,900,4;
     1735,900,5;
     875,800,6;
     1075,800,7;
     1300,800,8;
     1535,800,9;
     1735,800,10;
     875,700,11;
     1075,700,12;
     1300,700,13;
     1535,700,14;
     1735,700,15;
     875,600,16;
     1075,600,17;
     1300,600,18;
     1535,600,19;
     1735,600,20;
     ];
 s_points = [s_points(:,1)+20,s_points(:,2)+20];
 screen_points = [];
 screen_points = [2800-[s_points(:,1)],1500-[s_points(:,2)]];
 
ka = 1.0e+03 *[
    1.611196545804656   0.019434176103427   1.354552316941951
  -0.000000000000000   1.689040998351178   0.788870398926534
                   0                   0   0.001000000000000];
kb = 1.0e+03 *[
   1.588653821900676   0.022795451192475   1.306807887775449
                   0   1.693007153620145   0.855627364356785
                   0                   0   0.001000000000000]; 
               
dT = [0.999727734021476  -0.004120967273723  -0.022966833848283 0.068274560730787
   0.005181803444584   0.998913049386643   0.046323521866748 0.002030923927373
   0.022750972316528  -0.046429919166485   0.998662433390205 -0.005749898096829
                   0                   0                   0 1.000000000000000];
vec_depth = [0.36,0.4,0.43,0.47,0.5, 0.53,0.5,0.43,0.4,0.38, 0.4,0.43,0.46,0.49,0.51, 0.3,0.33,0.36,0.37,0.4];
vec_depth = vec_depth;
results = GetScreenPointsRight_fromLeft(screen_points(:,1:2),vec_depth,ka,kb,dT);
results = results';
screen_results = [2800-[results(:,1)],1500-[results(:,2)]];
screen_results(:,1) = screen_results(:,1)-20;
screen_results(:,2) = screen_results(:,2)-20;

function sp = GetScreenPointsRight_fromLeft(screen_pts_l, depths, K_l, K_r, dT)

%reconstruct 3D points:
Npoints = size(screen_pts_l,1);

P3D_n  = K_l\[screen_pts_l.' ; ones(1,Npoints)];
P3D_n = P3D_n./P3D_n(3,:);
P3D = depths.*P3D_n;

%represent in right camera:
P3D_r = dT * [P3D;ones(1,Npoints)];

%project on image:
sp = K_r * (P3D_r(1:3,:)./P3D_r(3,:));

end