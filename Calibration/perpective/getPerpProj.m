function [Ki,K,R,T] = getPerpProj(screen_points,world_points, n, f, width, height)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

[camMatrix,error] = estimateCameraMatrix(screen_points,world_points);

 if(camMatrix(1,1) > 0)
 M = -camMatrix';
 else
 M = camMatrix';
 end

%A = M(1:3,1:3);
%[Q,L] = qr(A^-1);
%K = L^-1;
%R = Q^-1;
%T = K^-1*M(:,4);

[K, Rc_w, Pc, pp, pv] = decomposecamera(M);

K = K/K(3,3);
R = Rc_w;
T = -Rc_w*Pc;

x0 = 0;%camera image origin
y0 = 0;
y_up_down = 1;%1 if y up and -1 if y down

Ki = [  
    2*K(1,1)/width, 0, 1.0+(-2.0*K(1,3) + 2*x0)/width, 0;
    0, y_up_down*-2*K(2,2)/height, (y_up_down*-2.0*K(2,3) + 2*y0)/height + y_up_down*1.0, 0;
    0, 0, (f+n)/(n-f), (2*f*n)/(n-f);
    0, 0, -1, 0
];

%This is to obtain the unity like matrix, with positive values
Ki(1,:) = -Ki(1,:);
Ki(2,:) = -Ki(2,:);
end

