clear;clc;
load('errorcube_jf.mat');
load('errorcube_avg_jf.mat');
load('errorcube_forced_jf.mat');
load('errorcube_fixed_jf.mat');
subjs = {'_jf','_jr','_cr'};

s=1;
alldata_r(1,:,:) = save_r;
alldata_r(2,:,:) = save_r_avg;
alldata_r(3,:,:) = save_r_forced;
alldata_r(4,:,:) = save_r_fixed;

alldata_t(1,:,:) = save_t;
alldata_t(2,:,:) = save_t_avg;
alldata_t(3,:,:) = save_t_forced;
alldata_t(4,:,:) = save_t_fixed;

%Rotation
colors = {'r','g','b','c'};
h = boxplot2(alldata_r);
for ii = 1:(size(alldata_r,1))
    structfun(@(x) set(x(:,ii), 'color', colors{ii}, 'markeredgecolor', colors{ii}), h);
end
set([h.lwhis h.uwhis], 'linestyle', '-');
set(h.out, 'marker', '.');
xticks(1:size(alldata_r,1))
xticklabels({'SPAAM','SPAAM_{avg}','SPAAM_{fact}','SPAAM_{fixed}'})
xlabel('Method');
ylabel('Rotation error (%)');
%legend('SPAAM','SPAAM_{avg}','SPAAM_{fact}','SPAAM_{fixed}');

fig = gcf;
fig.PaperUnits = 'inches';
fig.PaperPosition = [0 0 10 5];
print(['Figs/RotCubeError' subjs{s}],'-dpng','-r0')
close
%Translation
colors = {'r','g','b','c'};
h = boxplot2(alldata_t);
for ii = 1:(size(alldata_t,1))
    structfun(@(x) set(x(:,ii), 'color', colors{ii}, 'markeredgecolor', colors{ii}), h);
end
set([h.lwhis h.uwhis], 'linestyle', '-');
set(h.out, 'marker', '.');
xticks(1:size(alldata_t,1))
xticklabels({'SPAAM','SPAAM_{avg}','SPAAM_{fact}','SPAAM_{fixed}'})
xlabel('Method');
ylabel('Translation error (%)');
%legend('SPAAM','SPAAM_{avg}','SPAAM_{fact}','SPAAM_{fixed}');

fig = gcf;
fig.PaperUnits = 'inches';
fig.PaperPosition = [0 0 10 5];
print(['Figs/TranslCubeError' subjs{s}],'-dpng','-r0')

close


var_r = save_r;
var_t = save_t;

alldata_n_r(1,1,:) = [var_r(1,1:6)]%,var_r(2,1:6),var_r(3,1:6)];
alldata_n_r(2,1,:) = [var_r(1,7:12)]%,var_r(2,7:12),var_r(3,7:12)];
alldata_n_r(3,1,:) = [var_r(1,13:18)]%,var_r(2,13:18),var_r(3,13:18)];

%Rotation
colors = {'r','g','b','c'};
h = boxplot2(alldata_n_r);
for ii = 1:(size(alldata_n_r,1))
    structfun(@(x) set(x(:,ii), 'color', colors{ii}, 'markeredgecolor', colors{ii}), h);
end
set([h.lwhis h.uwhis], 'linestyle', '-');
set(h.out, 'marker', '.');
xticks(1:size(alldata_r,1))
xticklabels({'Center','Periphery','Free Move'})
xlabel('Method');
ylabel('Rotation error (%)');
ylim([0 6]);

fig = gcf;
fig.PaperUnits = 'inches';
fig.PaperPosition = [0 0 5 3];
print(['Figs/SPAAMROTCubeError' subjs{s}],'-dpng','-r0')
close


alldata_n_t(1,1,:) = [var_t(1,1:6)]%,var_t(2,1:6),var_t(3,1:6)];
alldata_n_t(2,1,:) = [var_t(1,7:12)]%,var_t(2,7:12),var_t(3,7:12)];
alldata_n_t(3,1,:) = [var_t(1,13:18)]%,var_t(2,13:18),var_t(3,13:18)];

% alldata_n_t(1,1,:) = var_t(1,1:6);
% alldata_n_t(2,1,:) = var_t(1,7:12);
% alldata_n_t(3,1,:) = var_t(1,13:18);
% alldata_n_t(1,2,:) = var_t(2,1:6);
% alldata_n_t(2,2,:) = var_t(2,7:12);
% alldata_n_t(3,2,:) = var_t(2,13:18);
% alldata_n_t(1,3,:) = var_t(3,1:6);
% alldata_n_t(2,3,:) = var_t(3,7:12);
% alldata_n_t(3,3,:) = var_t(3,13:18);

%Translation
colors = {'r','g','b','c'};
h = boxplot2(alldata_n_t);
for ii = 1:(size(alldata_n_t,1))
    structfun(@(x) set(x(:,ii), 'color', colors{ii}, 'markeredgecolor', colors{ii}), h);
end
set([h.lwhis h.uwhis], 'linestyle', '-');
set(h.out, 'marker', '.');
xticks(1:size(alldata_r,1))
xticklabels({'Center','Periphery','Free Move'})
xlabel('Alignment');
ylabel('Translation error (%)');
ylim([0 30]);

fig = gcf;
fig.PaperUnits = 'inches';
fig.PaperPosition = [0 0 5 3];
print(['Figs/SPAAMTRANSLCubeError' subjs{s}],'-dpng','-r0')
close